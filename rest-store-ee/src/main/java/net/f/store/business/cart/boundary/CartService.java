package net.f.store.business.cart.boundary;

import net.f.store.business.cart.entity.Cart;
import net.f.store.business.cart.entity.Cart_;
import net.f.store.business.cart.entity.LineItem;
import net.f.store.business.cart.entity.LineItemDTO;
import net.f.store.business.core.boundary.Service;
import net.f.store.business.customers.entity.Customer;
import net.f.store.business.customers.entity.Customer_;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;

/**
 * @author Artyom Fyodorov
 */
@Stateless
public class CartService extends Service<Cart, String> {
    private @Inject EntityManager em;
    private @Inject LineItemsService lineItemsService;

    CartService() {
        super(Cart.class);
    }

    @Override
    public Optional<Cart> findOne(String username) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Cart> cq = cb.createQuery(Cart.class);
        Root<Cart> sc = cq.from(Cart.class);
        sc.fetch(Cart_.lineItems, JoinType.LEFT);
        cq.select(sc);
        Path<Customer> c = sc.get(Cart_.customer);
        Predicate customerEmail = cb.equal(c.get(Customer_.email), username);
        cq.where(customerEmail);

        TypedQuery<Cart> tq = this.em.createQuery(cq);
        return uniqueResult(tq);
    }

    public Cart saveToCart(Customer customer, LineItemDTO dto) {
        Cart cart = customer.getCart();
        if (cart == null) {
            cart = new Cart();
            cart.setCustomer(customer);
        }

        LineItem entity = dtoToEntity(dto);
        cart.addLineItem(entity);
        return this.save(cart);
    }

    public void removeFromCart(Customer customer, LineItem lineItem) {
        ofNullable(customer.getCart()).ifPresent(cart -> {
            List<LineItem> lineItems = cart.getLineItems();
            if (lineItems.size() > 1) {
                lineItems.remove(lineItem);
                this.em.merge(cart);
            } else {
                customer.setCart(null);
                this.delete(cart);
            }
        });
    }

    public void updateLineItem(Customer customer, LineItem lineItem, LineItemDTO dto) {
        int quantity = dto.getQuantity();
        if (quantity == 0) {
            removeFromCart(customer, lineItem);
        } else {
            lineItem.setQuantity(quantity);
            lineItemsService.update(lineItem);
        }
    }

    private LineItem dtoToEntity(LineItemDTO dto) {
        LineItem entity = new LineItem();
        entity.setQuantity(dto.getQuantity());
        entity.setProduct(dto.getProduct());

        return entity;
    }

    @Override
    protected EntityManager getEM() {
        return this.em;
    }
}
