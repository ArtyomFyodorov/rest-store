package net.f.store.business.orders.boundary;

import net.f.store.business.core.boundary.JsonBuilder;
import net.f.store.business.core.boundary.PagingParams;
import net.f.store.business.customers.boundary.CustomersService;
import net.f.store.business.customers.entity.Customer;
import net.f.store.business.customers.entity.Role;
import net.f.store.business.orders.entity.Order;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * @author Artyom Fyodorov
 */
public class OrdersResourceBean implements OrdersResource {
    private static final String BAD_REQUEST_EXCEPTION_MESSAGE = "Bad Parameters: %nstart %d %nsize %d";
    private static final String CART_BAD_REQUEST_EXCEPTION_MESSAGE = "Your shopping cart is empty";
    private static final String NOT_FOUND_EXCEPTION_MESSAGE = "Could not get resource for path parameter: %d";
    @Inject OrdersService ordersService;
    @Inject CustomersService customersService;
    @Context UriInfo info;
    @Context Request request;
    @Context SecurityContext securityContext;
    @Inject JsonBuilder jsonBuilder;

    @Override
    public Response checkout() {
        Order newOrder = this.ordersService.makeOrder(getAuthCustomer());
        if (newOrder == null) {
            throw new BadRequestException(CART_BAD_REQUEST_EXCEPTION_MESSAGE);
        }
        String orderId = String.valueOf(newOrder.getId());
        URI uri = info.getAbsolutePathBuilder().path(orderId).build();

        return Response.created(uri).build();
    }

    @Override
    public Response getOne(Long orderId) {
        Order order = getOrder(orderId);

        String orderVersion = String.valueOf(order.getVersion());
        EntityTag eTag = new EntityTag(orderVersion);
        Response.ResponseBuilder rb = request.evaluatePreconditions(eTag);
        if (rb == null) {
            JsonObject orderJson = jsonBuilder.buildOrder(order, info);
            rb = Response.ok(orderJson).tag(eTag);
        }
        CacheControl cc = new CacheControl();
        cc.setMaxAge(86_400); // 24 h (hour)

        return rb.build();
    }

    @Override
    public JsonObject getAll(PagingParams pagingParams) {
        if (pagingParams.start > 0 && pagingParams.size < 1) {
            throw new BadRequestException(
                    format(BAD_REQUEST_EXCEPTION_MESSAGE, pagingParams.start, pagingParams.size));
        }
        List<Order> orderList = getOrders(pagingParams);

        if (orderList == null || orderList.isEmpty()) {
            return null; //  204 No Content
        } else {
            return jsonBuilder.buildOrders(orderList, pagingParams, info);
        }
    }

    private Customer getAuthCustomer() {
        return customersService.findByEmail(
                securityContext.getUserPrincipal().getName())
                .orElseThrow(() -> new ServerErrorException(INTERNAL_SERVER_ERROR));
    }

    private Order getOrder(Long orderId) {
        String userPrincipalName = securityContext.getUserPrincipal().getName();
        Optional<Order> maybeOrder =
                securityContext.isUserInRole(Role.ADMIN.toString())
                        ? ordersService.findOne(orderId)
                        : ordersService.findOrderOfCurrentCustomer(orderId, userPrincipalName);

        return maybeOrder.orElseThrow(() -> new NotFoundException(
                format(NOT_FOUND_EXCEPTION_MESSAGE, orderId)));
    }

    private List<Order> getOrders(PagingParams pagingParams) {
        String userPrincipalName = securityContext.getUserPrincipal().getName();

        return securityContext.isUserInRole(Role.ADMIN.toString())
                ? ordersService.allByCustomerUsername(pagingParams.start, pagingParams.size)
                : ordersService.allOrdersOfTheCurrentCustomer(
                userPrincipalName, pagingParams.start, pagingParams.size);
    }
}
