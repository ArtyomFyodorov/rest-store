package net.f.store.business.core;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

/**
 * @author Artyom Fyodorov
 */
public class ConflictException extends ClientErrorException {
    private static final long serialVersionUID = -8272453707692264335L;

    public ConflictException(String message) {
        super(message, Response.Status.CONFLICT);
    }

}
