package net.f.store.business.security.control;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * The {@code Decoder} that decodes using the Basic type base64 encoding scheme.
 *
 * @author Artyom Fyodorov
 */
public final class Decoder {

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    private Decoder() {
    }

    /**
     * Decodes a {@code Base64} encoded value of the
     * <code><a href="https://tools.ietf.org/html/rfc7617#section-2">Authorization</a></code>
     * field into a newly-allocated {@code String} array using
     * the {@link java.util.Base64} encoding scheme.
     *
     * @param authHeaderVal The string to decode
     * @return A newly-allocated {@code String} array containing
     * the decoded strings.
     */
    public static String[] decode(String authHeaderVal) {
        return decode(authHeaderVal, DEFAULT_CHARSET);
    }

    /**
     * Decodes a {@code Base64} encoded value of the
     * <code><a href="https://tools.ietf.org/html/rfc7617#section-2">Authorization</a></code>
     * field into a newly-allocated {@code String} array using
     * the {@link java.util.Base64} encoding scheme.
     *
     * @param authHeaderVal The string to decode
     * @param charset       The {@linkplain java.nio.charset.Charset charset}
     *                      to be used to decode the bytes
     * @return A newly-allocated {@code String} array containing
     * the decoded strings.
     */
    public static String[] decode(String authHeaderVal, Charset charset) {
        if (authHeaderVal == null || "".equals(authHeaderVal)) {
            throw new IllegalArgumentException("Invalid authentication header value");
        }
        if (charset == null) {
            throw new IllegalArgumentException("Charset is not provided");
        }

        String credentials = authHeaderVal.substring("Basic".length()).trim();   //  minus prefix 'Basic '
        Base64.Decoder base64Decoder = Base64.getDecoder();
        credentials = new String(base64Decoder.decode(credentials), charset);

        return credentials.split(":");
    }
}
