package net.f.store.business.customers.entity;

/**
 * @author Artyom Fyodorov
 */
public enum Role {
    USER, ADMIN
}
