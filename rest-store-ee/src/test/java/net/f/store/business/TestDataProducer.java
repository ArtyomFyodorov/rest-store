package net.f.store.business;

import net.f.store.business.cart.entity.Cart;
import net.f.store.business.cart.entity.LineItem;
import net.f.store.business.customers.entity.Address;
import net.f.store.business.customers.entity.Customer;
import net.f.store.business.customers.entity.Role;
import net.f.store.business.orders.entity.Order;
import net.f.store.business.products.entity.Product;

import java.util.Arrays;
import java.util.List;

/**
 * @author Artyom Fyodorov
 */
public class TestDataProducer {

    public static Cart getCartWithItems() {
        List<LineItem> lineItems = getLineItems();

        Cart cart = new Cart();
        cart.setLineItems(lineItems);

        return cart;
    }

    public static Order getOrder() {
        Customer customer = getCustomer();
        List<LineItem> lineItems = getCartWithItems().getLineItems();

        Order order = new Order();
        order.setLineItems(lineItems);
        order.setCustomer(customer);

        return order;
    }

    public static Address getAddress() {
        Address address = new Address();
        address.setStreet("green");
        address.setCity("sim");
        address.setState("hot");
        address.setZipcode("180102");
        address.setCountry("world");

        return address;
    }

    public static Customer getCustomer() {
        Customer customer = new Customer();
        Address address = getAddress();

        customer.setFirstName("John");
        customer.setLastName("Smith");
        customer.setEmail("jsmith" + System.identityHashCode(customer) + "@mail.com");
        customer.setPhoneNumber("+7 1234567890");
        customer.setPassword("Qwerty123");
        customer.setAddress(address);
        customer.setRole(Role.USER);

        return customer;
    }

    public static List<LineItem> getLineItems() {
        Product shorts = getProduct(ProductType.CLOTHING_SHORTS);
        Product sunglasses = getProduct(ProductType.SUNGLASSES);

        LineItem lineItemShorts = new LineItem(3, shorts);
        LineItem lineItemGlasses = new LineItem(2, sunglasses);

        return Arrays.asList(lineItemShorts, lineItemGlasses);
    }

    private static Product getSunglasses() {
        Product sunglasses = new Product();
        sunglasses.setName("sunglasses");
        sunglasses.setPrice(10.99);
        sunglasses.setAvailableQuantity(5);
        return sunglasses;
    }

    private static Product getShorts() {
        Product shorts = new Product();
        shorts.setName("shorts");
        shorts.setPrice(15.50);
        shorts.setAvailableQuantity(10);
        return shorts;
    }

    public static Product getProduct(ProductType type) {
        switch (type) {
            case SUNGLASSES:
                return getSunglasses();
            case CLOTHING_SHORTS:
                return getShorts();
            default:
                throw new IllegalArgumentException("Product not found: " + type);
        }
    }

    public enum ProductType {
        CLOTHING_SHORTS, SUNGLASSES
    }

}
