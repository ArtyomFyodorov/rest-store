package net.f.store.business.security.boundary;

import net.f.store.business.core.entity.ErrorMessage;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Artyom Fyodorov
 */
@Provider
public class ForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {
    @Override
    public Response toResponse(ForbiddenException ex) {
        int statusCode = ex.getResponse().getStatus();
        ErrorMessage errorMessage = ErrorMessage.of(ex.getLocalizedMessage(), statusCode);
        return Response.status(statusCode).entity(errorMessage).build();
    }
}
