package net.f.store.business;

import java.util.Base64;

/**
 * @author Artyom Fyodorov
 */
public final class BasicEncoder {
    private static final String PREFIX_AUTHORIZATION_HEADER_VALUE = "Basic ";

    private BasicEncoder() {
    }

    /**
     * Encodes the username and password into a String using the
     * {@link java.util.Base64} encoding scheme.
     *
     * <p>This method first encodes all input strings into a base64 encoded
     * byte array and then constructs a new {@code String} by using
     * prefix '{@code Basic} ' plus the encoded byte array and the
     * {@link java.nio.charset.StandardCharsets#ISO_8859_1} charset.
     *
     * <p>In other words, an invocation of this method has exactly
     * the same effect as invoking
     * {@code new String("Basic " + encode(src), StandardCharsets.ISO_8859_1)}.
     *
     * @param username the username to encode
     * @param password the password to encode
     * @return A String containing the resulting Base64 encoded characters
     */
    public static String toAuthorizationHeaderValue(String username, String password) {
        String credentials = username + ":" + password;
        byte[] rawData = credentials.getBytes();

        return PREFIX_AUTHORIZATION_HEADER_VALUE
                + Base64.getEncoder().encodeToString(rawData);
    }
}
