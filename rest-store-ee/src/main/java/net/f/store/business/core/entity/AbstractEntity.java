package net.f.store.business.core.entity;

import javax.persistence.*;

/**
 * @author Artyom Fyodorov
 */
@MappedSuperclass
public class AbstractEntity {

    private @Id @GeneratedValue(strategy = GenerationType.AUTO) Long id;
    private @Version int version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // when the entity id is null, we can guarantee equality
    // only for the same object references.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractEntity)) return false;

        AbstractEntity that = (AbstractEntity) o;

        if (version != that.version) return false;
        return id != null && id.equals(that.id);
    }

    /*
    This is a terrible implementation in terms of performance HashSet or HashMap,
    but we should never fetch thousands of entities in a @OneToMany Set
    because the performance penalty on the database side is multiple orders of magnitude
    higher than using a single hashed bucket.
    */
    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return String.format("id=%d, version=%d", id, version);
    }
}

