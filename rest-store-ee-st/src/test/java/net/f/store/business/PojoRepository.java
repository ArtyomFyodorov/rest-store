package net.f.store.business;

import net.f.store.business.cart.Cart;
import net.f.store.business.cart.LineItem;
import net.f.store.business.cart.LineItemDTO;
import net.f.store.business.customers.Address;
import net.f.store.business.customers.Customer;
import net.f.store.business.orders.Order;
import net.f.store.business.products.Product;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;

/**
 * @author Artyom Fyodorov
 */
public final class PojoRepository {
    private static final String DEFAULT_CUSTOMER_NAME = "dummy";
    private static final String DEFAULT_PRODUCT_NAME = "shield";
    private static final Map<String, Customer> customers = new HashMap<>();
    private static final Map<String, Product> products = new HashMap<>();

    static {
        addDefaultCustomer();
        addDefaultProduct();
    }

    private PojoRepository() {
    }

    private static void addDefaultCustomer() {
        Address address = new Address("Green", "Enterprise", "Java", "010101", "World");
        Customer customer = new Customer("John", "Doe", "integration@test.com", "IntegrationTest42", "+7 1234567890");
        customer.setAddress(address);
        addCustomer(DEFAULT_CUSTOMER_NAME, customer);
    }

    private static void addDefaultProduct() {
        Product product = new Product("shield", 249.99, 100);
        addProduct(DEFAULT_PRODUCT_NAME, product);
    }

    public static void addCustomer(String name, Customer customer) {
        customers.put(name, customer);
    }

    public static void addProduct(String name, Product product) {
        products.put(name, product);
    }

    public static Customer getCustomerWithRandomEmail() {
        final Customer customer = getCustomer(DEFAULT_CUSTOMER_NAME);
        final String localPart = RandomGenerator.getAlphanumericString();

        customer.setEmail(localPart + "@example.com");

        return customer;
    }

    public static Product getProduct() {
        return getProduct(DEFAULT_PRODUCT_NAME);
    }


    public static Customer getCustomer(String name) {
        Customer c = customers.get(name);
        Optional<Customer> maybeCustomer = ofNullable(c);

        return maybeCustomer.orElseThrow(() ->
                new IllegalArgumentException("No customer with name: " + name));
    }

    public static Product getProduct(String name) {
        Product p = products.get(name);
        Optional<Product> maybeProduct = ofNullable(p);

        return maybeProduct.orElseThrow(() ->
                new IllegalArgumentException("No product with name: " + name)
        );
    }


    private static List<LineItem> lineItems() {
        addDefaultProduct();
        Product shield = getProduct(DEFAULT_PRODUCT_NAME);

        LineItem lineItemSocks = new LineItem();
        lineItemSocks.setQuantity(6);
        lineItemSocks.setProduct(shield);

        return singletonList(lineItemSocks);

    }

    public static JsonObject getCustomerAsJson() {
        return getCustomerAsJson(DEFAULT_CUSTOMER_NAME);
    }

    //  ============================================================================================================

    public static JsonObject getCustomerAsJson(String name) {
        return convertToJsonObject(getCustomer(name));
    }

    public static JsonObject getCustomerAsJson(Customer customer) {
        return convertToJsonObject(customer);
    }

    public static List<Customer> getJsonAsCustomerList(JsonObject jsonObject) {
        return convertToCustomerList(jsonObject);
    }

    public static Customer getJsonAsCustomer(JsonObject jsonObject) {
        return convertToCustomer(jsonObject);
    }

    public static Address getJsonAsAddress(JsonObject jsonObject) {
        return convertToAddress(jsonObject);
    }

    public static JsonObject getProductAsJson(Product product) {
        return convertToJsonObject(product);
    }

    public static JsonObject getLineItemAsJson(LineItemDTO lineItem) {
        return convertToJsonObject(lineItem);
    }

    public static Order getJsonAsOrder(JsonObject orderJson) {
        return convertToOrder(orderJson);
    }

    public static Product getJsonAsProduct(JsonObject productJson) {
        return convertToProduct(productJson);
    }

    public static Cart getJsonAsCart(JsonObject cartJson) {
        return convertToCart(cartJson);
    }

    private static JsonObject convertToJsonObject(Product product) {
        return Json.createObjectBuilder()
                .add("name", product.getName())
                .add("price", product.getPrice())
                .add("available", product.getAvailableQuantity())
                .build();
    }

    //  ============================================================================================================

    private static JsonObject convertToJsonObject(LineItemDTO lineItem) {
        return Json.createObjectBuilder()
                .add("quantity", lineItem.getQuantity())
                .add("product-code", lineItem.getProductId())
                .build();
    }

    private static JsonObject convertToJsonObject(Customer customer) {
        Address address = customer.getAddress();

        return Json.createObjectBuilder()
                .add("first-name", customer.getFirstName())
                .add("last-name", customer.getLastName())
                .add("email", customer.getEmail())
                .add("password", customer.getPassword())
                .add("phone-number", customer.getPhoneNumber())
                .add("street", address.getStreet())
                .add("city", address.getCity())
                .add("state", address.getState())
                .add("zipcode", address.getZipcode())
                .add("country", address.getCountry())
                .build();
    }

    private static Order convertToOrder(JsonObject json) {
        Order order = new Order();

        json.values().stream()
                .filter(value -> value.getValueType().equals(ValueType.OBJECT))
                .map(JsonValue::asJsonObject)
                .filter(object -> object.containsKey("grand-total"))
                .findAny().ifPresent(object ->
                order.setGrandTotal(object.getJsonNumber("grand-total").doubleValue()));

        List<LineItem> lineItems = convertToLineItemList(json);
        order.setLineItems(lineItems);

        System.out.println(order);
        return order;
    }

    private static Cart convertToCart(JsonObject json) {
        Cart cart = new Cart();

        JsonObject properties = json.getJsonObject("properties");
        ofNullable(properties)
                .filter(object -> object.containsKey("cart-subtotal"))
                .ifPresent(object ->
                        cart.setCartSubtotal(object.getJsonNumber("cart-subtotal").doubleValue()));

        List<LineItem> lineItems = convertToLineItemList(json);
        cart.setLineItems(lineItems);

        System.out.println(cart);
        return cart;
    }

    private static List<LineItem> convertToLineItemList(JsonObject json) {
        List<LineItem> result = json.getJsonArray("entities").stream()
                .filter(value -> value.getValueType().equals(ValueType.OBJECT))
                .map(JsonValue::asJsonObject)
                .filter(object -> object.containsKey("class"))
                .filter(object -> object.containsKey("properties"))
                .map(PojoRepository::convertToLineItem)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());


        System.out.println(result);
        return result.isEmpty() ? null : result;
    }

    private static LineItem convertToLineItem(JsonObject json) {
        for (JsonValue value : json.values()) {
            if (value.getValueType().equals(ValueType.OBJECT)) {
                JsonObject object = value.asJsonObject();
                if (object.containsKey("quantity")) {
                    LineItem lineItem = new LineItem();
                    lineItem.setQuantity(object.getInt("quantity"));
                    object.values().stream()
                            .filter(jv -> jv.getValueType().equals(ValueType.ARRAY))
                            .flatMap(jv -> jv.asJsonArray().stream())
                            .filter(jv -> jv.getValueType().equals(ValueType.OBJECT))
                            .map(jv -> convertToProduct(jv.asJsonObject()))
                            .findAny().ifPresent(lineItem::setProduct);

                    System.out.println(lineItem);
                    return lineItem;
                }
            }
        }

        return null;
    }

    private static Product convertToProduct(JsonObject json) {
        if (json.containsKey("properties")) {
            JsonObject properties = json.getJsonObject("properties");
            if (properties.containsKey("name") && properties.containsKey("price")) {
                Product product = new Product();
                product.setName(properties.getString("name"));
                product.setPrice(properties.getJsonNumber("price").doubleValue());
                System.out.println(product);
                return product;
            }
        }

        return null;
    }

    private static List<Customer> convertToCustomerList(JsonObject json) {
        return json.values().stream()
                .filter(value -> value.getValueType().equals(ValueType.ARRAY))
                .flatMap(value -> value.asJsonArray().stream())
                .filter(value -> value.getValueType().equals(ValueType.OBJECT))
                .map(value -> convertToCustomer(value.asJsonObject()))
                .collect(Collectors.toList());
    }

    private static Customer convertToCustomer(JsonObject json) {
        for (JsonValue value : json.values()) {
            if (value.getValueType().equals(ValueType.OBJECT)) {
                JsonObject jsonObject = value.asJsonObject();
                if (jsonObject.containsKey("first-name")
                        && jsonObject.containsKey("last-name")
                        && jsonObject.containsKey("email")
                        && jsonObject.containsKey("phone-number")) {
                    Customer result = new Customer();
                    result.setFirstName(jsonObject.getString("first-name"));
                    result.setLastName(jsonObject.getString("last-name"));
                    result.setEmail(jsonObject.getString("email"));
                    result.setPhoneNumber(jsonObject.getString("phone-number"));
                    result.setAddress(convertToAddress(json));
                    return result;
                } else {
                    return convertToCustomer(jsonObject);
                }
            }
        }

        return null;
    }

    private static Address convertToAddress(JsonObject json) {
        for (JsonValue value : json.values()) {
            if (value.getValueType().equals(ValueType.OBJECT)) {
                JsonObject jsonObject = value.asJsonObject();
                if (jsonObject.containsKey("street")
                        && jsonObject.containsKey("city")
                        && jsonObject.containsKey("state")
                        && jsonObject.containsKey("country")
                        && jsonObject.containsKey("zipcode")) {
                    Address result = new Address();
                    result.setStreet(jsonObject.getString("street"));
                    result.setCity(jsonObject.getString("city"));
                    result.setCountry(jsonObject.getString("state"));
                    result.setState(jsonObject.getString("zipcode"));
                    result.setZipcode(jsonObject.getString("country"));
                    return result;
                } else {
                    return convertToAddress(jsonObject);
                }
            }
        }

        return null;
    }

    //  based on http://stackoverflow.com/a/41156
    public static final class RandomGenerator {
        private static final SecureRandom RANDOM = new SecureRandom();

        static String getAlphanumericString() {
            return new BigInteger(130, RANDOM).toString(32);
        }
    }
}
