package net.f.store.business.orders;

import net.f.store.business.cart.LineItem;

import java.util.List;
import java.util.Objects;

/**
 * @author Artyom Fyodorov
 */
public class Order {

    private List<LineItem> lineItems;
    private double grandTotal;

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Double.compare(order.grandTotal, grandTotal) == 0 &&
                Objects.equals(lineItems, order.lineItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lineItems, grandTotal);
    }

    @Override
    public String toString() {
        return "Order{" +
                "lineItems=" + lineItems +
                ", grandTotal=" + grandTotal +
                '}';
    }
}
