package net.f.store.business.customers.entity;

import net.f.store.business.AbstractValidatorIT;
import net.f.store.business.TestDataProducer;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static java.util.Collections.nCopies;
import static java.util.stream.Collectors.joining;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class CustomerIT extends AbstractValidatorIT {

    private Customer cut;

    @Before
    public void initCustomer() throws Exception {
        this.cut = TestDataProducer.getCustomer();
    }

    @Test
    public void noConstraintViolation() throws Exception {
        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void firstNameLowerThanSpecifiedMinimum() throws Exception {
        this.cut.setFirstName("");

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void firstNameGreaterThanSpecifiedMaximum() throws Exception {
        String string = String.valueOf(new char[65]);
        this.cut.setFirstName(string);

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void lastNameLowerThanSpecifiedMinimum() throws Exception {
        this.cut.setLastName("");

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void lastNameGreaterThanSpecifiedMaximum() throws Exception {
        String string = String.valueOf(new char[65]);
        this.cut.setLastName(string);

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void phoneNumberWithoutCountryCode() throws Exception {
        this.cut.setPhoneNumber("1234567890");

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void emptyPhoneNumber() throws Exception {
        this.cut.setPhoneNumber("");

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void phoneNumberIsNull() throws Exception {
        this.cut.setPhoneNumber(null);

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void emailWithoutAt() throws Exception {
        this.cut.setEmail("someonenowhere.com");

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void emailAddressLowerThanSpecifiedMinimum() throws Exception {
        this.cut.setPassword("some");
        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void emailAddressMoreThanSpecifiedMaximum() throws Exception {
        String email = nCopies(100, "a").stream()
                .collect(joining("", "some@", ".com"));
        this.cut.setEmail(email);
//        System.out.println(email);

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void passwordLowerThanSpecifiedMinimum() throws Exception {
        this.cut.setPassword("foo");

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void passwordMoreThanSpecifiedMaximum() throws Exception {
        String string = String.valueOf(new char[129]);
        this.cut.setPassword(string);

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void emptyAddress() throws Exception {
        this.cut.setAddress(new Address());

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 4;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void firstNameEmailPasswordAndAddressAreNull() throws Exception {
        this.cut = new Customer();

        Set<ConstraintViolation<Customer>> result = this.vut.validate(this.cut);
//        displayConstraintViolations(result);

        int expected = 5;
        assertThat(result.size(), is(expected));
    }
}