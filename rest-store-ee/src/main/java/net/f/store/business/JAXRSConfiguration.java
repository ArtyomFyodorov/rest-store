package net.f.store.business;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Artyom Fyodorov
 */
@ApplicationPath("resources")
public class JAXRSConfiguration extends Application {
}
