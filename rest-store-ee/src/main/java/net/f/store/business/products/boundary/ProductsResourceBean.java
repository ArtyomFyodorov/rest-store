package net.f.store.business.products.boundary;

import net.f.store.business.core.boundary.JsonBuilder;
import net.f.store.business.core.boundary.PagingParams;
import net.f.store.business.products.entity.Product;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

import static java.lang.String.format;
import static java.lang.String.valueOf;

/**
 * @author Artyom Fyodorov
 */
public class ProductsResourceBean implements ProductsResource {
    private static final String NOT_FOUND_EXCEPTION_MESSAGE = "Could not get resource for path parameter: %d";
    private static final String BAD_REQUEST_EXCEPTION_MESSAGE = "Bad Parameters: %nstart %d %nsize %d";

    @Context UriInfo info;
    @Context Request request;
    @Inject JsonBuilder jsonBuilder;
    @Inject ProductsService productsService;

    @Override
    public Response create(Product product) {
        Product managedProduct = productsService.save(product);
        String productId = managedProduct.getId().toString();
        URI uri = info.getAbsolutePathBuilder().path(productId).build();

        return Response.created(uri).build();
    }

    @Override
    public Response getOne(Long productId, Request request) {
        Product product = getProduct(productId);

        final int productVersion = product.getVersion();
        EntityTag eTag = new EntityTag(valueOf(productVersion));
        Response.ResponseBuilder rb = request.evaluatePreconditions(eTag);
        if (rb == null) {
            JsonObject productJson = jsonBuilder.buildProduct(product, info, product.isAvailableForOrder());
            rb = Response.ok(productJson);
            rb.tag(eTag);
        }
        CacheControl cc = new CacheControl();
        cc.setMaxAge(86_400); // 24 h (hour)

        return rb.cacheControl(cc).build();
    }

    @Override
    public JsonObject getAll(String name, PagingParams pagingParams) {
        if (pagingParams.start > 0 && pagingParams.size < 1) {
            throw new BadRequestException(format(BAD_REQUEST_EXCEPTION_MESSAGE,
                    pagingParams.start, pagingParams.size));
        }

        List<Product> productList = productsService.allByName(name, pagingParams.start, pagingParams.size);
        if (productList == null || productList.isEmpty()) {
            return null;    //  204 No Content
        } else {
            return jsonBuilder.buildProducts(productList, pagingParams, info);
        }
    }

    private Product getProduct(Long productId) {
        return productsService.findOne(productId)
                .orElseThrow(() -> new NotFoundException(format(NOT_FOUND_EXCEPTION_MESSAGE, productId)));
    }
}
