package net.f.store.business.core;

import net.f.store.business.core.entity.ErrorMessage;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.stream.Collectors;

/**
 * @author Artyom Fyodorov
 */
@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
    @Override
    public Response toResponse(ConstraintViolationException ex) {
        String message = ex.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining("; "));
        int statusCode = Response.Status.BAD_REQUEST.getStatusCode();
        ErrorMessage errorMessage = ErrorMessage.of(message, statusCode);

        return Response.status(statusCode).entity(errorMessage).build();
    }
}
