create sequence hibernate_sequence start with 100 increment by 1/*it is important*/
create table customers (id bigint not null, version integer not null, city varchar(100) not null, country varchar(100) not null, state varchar(100), street varchar(100) not null, zip_code varchar(30) not null, email varchar(100) not null, first_name varchar(64) not null, last_name varchar(64), password varchar(128) not null, phone_number varchar(14) not null, role varchar(64), primary key (id))
create table jng_cart_line_item (cart_fk bigint not null, line_item_fk bigint not null)
create table jng_order_line_item (order_fk bigint not null, line_item_fk bigint not null)
create table line_items (id bigint not null, version integer not null, quantity integer not null check (quantity>=1), product_fk bigint not null, primary key (id))
create table orders (id bigint not null, version integer not null, creation_date timestamp, grand_total double, customer_fk bigint not null, primary key (id))
create table products (id bigint not null, version integer not null, name varchar(255) not null, price double not null, available integer not null, primary key (id))
create table shopping_cart (id bigint not null, version integer not null, cart_subtotal double, customer_id bigint not null, primary key (id))
alter table customers add constraint UK_customers_1 unique (email)
alter table jng_cart_line_item add constraint UK_jng_cart_line_item_1 unique (line_item_fk)
alter table jng_order_line_item add constraint UK_jng_order_line_item_1 unique (line_item_fk)
alter table shopping_cart add constraint UK_shopping_cart_1 unique (customer_id)
alter table jng_cart_line_item add constraint FK_jng_cart_line_item_1 foreign key (line_item_fk) references line_items
alter table jng_cart_line_item add constraint FK_jng_cart_line_item_2 foreign key (cart_fk) references shopping_cart
alter table jng_order_line_item add constraint FK_jng_order_line_item_1 foreign key (line_item_fk) references line_items
alter table jng_order_line_item add constraint FK_jng_order_line_item_2 foreign key (order_fk) references orders
alter table line_items add constraint FK_line_items_1 foreign key (product_fk) references products
alter table orders add constraint FK_orders_1 foreign key (customer_fk) references customers
alter table shopping_cart add constraint FK_shopping_cart_1 foreign key (customer_id) references customers
