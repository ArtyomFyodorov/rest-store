package net.f.store.business.core.entity;

import javax.xml.bind.annotation.*;

/**
 * @author Artyom Fyodorov
 */
@XmlRootElement(name = "error-message")
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressWarnings("unused")
public class ErrorMessage {
    private String reason;
    @XmlElement(name = "status-code")
    private int code;
    @XmlTransient
    private String documentation;

    public ErrorMessage() {
    }

    private ErrorMessage(String reason, int code) {
        this.reason = reason;
        this.code = code;
    }

    public static ErrorMessage of(String reason, int code) {
        return new ErrorMessage(reason, code);
    }
}
