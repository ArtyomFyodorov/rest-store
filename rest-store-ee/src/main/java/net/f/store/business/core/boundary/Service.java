package net.f.store.business.core.boundary;

import net.f.store.business.logging.boundary.Loggable;

import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
public abstract class Service<T, U extends Serializable> {
    private final Class<T> entityClass;

    public Service(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected static <T> Optional<T> uniqueResult(TypedQuery<T> query) {
        List<T> results = query.setMaxResults(2).getResultList();
        if (results.size() > 1) {
            throw new NonUniqueResultException();
        }
        return results.isEmpty() ? Optional.empty() : Optional.ofNullable(results.get(0));
    }

    protected abstract EntityManager getEM();

    @Loggable
    public T save(T entity) {
        return getEM().merge(entity);
    }

    @Loggable
    public void delete(T deletableEntity) {
        getEM().remove(getEM().merge(deletableEntity));
    }

    public Optional<T> findOne(U id) {
        return Optional.ofNullable(getEM().find(entityClass, id));
    }
}
