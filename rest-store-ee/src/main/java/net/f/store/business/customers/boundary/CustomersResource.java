package net.f.store.business.customers.boundary;

import net.f.store.business.core.boundary.PagingParams;
import net.f.store.business.customers.entity.CustomerDTO;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.json.JsonObject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author Artyom Fyodorov
 */
@Path("/customers")
@Produces({"application/json", "application/vnd.siren+json"})
@Consumes({"application/json", "application/vnd.siren+json"})
@RolesAllowed({"ADMIN", "USER"})
public interface CustomersResource {

    @PermitAll
    @POST
    Response save(@NotNull @Valid CustomerDTO dto);

    @RolesAllowed({"ADMIN"})
    @GET
    @Path("/{id:[0-9][0-9]*}")
    Response getOne(@PathParam("id") Long requestId);

    @GET
    @Path("/me")
    Response getCurrentCustomer();

    @RolesAllowed("ADMIN")
    @GET
    JsonObject getAll(@BeanParam PagingParams pagingParams);

    @PUT
    @Path("/{id:[0-9][0-9]*}")
    Response update(@PathParam("id") Long requestId,
                    @NotNull @Valid CustomerDTO dto);

    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    void delete(@PathParam("id") Long requestId);

}
