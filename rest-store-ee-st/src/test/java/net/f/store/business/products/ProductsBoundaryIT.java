package net.f.store.business.products;

import net.f.store.business.AbstractClient;
import net.f.store.business.BasicEncoder;
import net.f.store.business.PojoRepository;
import net.f.store.business.customers.Customer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonObject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Response;
import java.net.URI;

import static javax.ws.rs.core.HttpHeaders.IF_NONE_MATCH;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;


/**
 * @author Artyom Fyodorov
 */
public class ProductsBoundaryIT extends AbstractClient {
    private Product dummyProduct;
    private String credentials;
    private URI productsLocation;
    private URI customerLocation;
    private URI productLocation;
    private JsonObject dummyProductJson;

    @Before
    public void init() throws Exception {
        this.customerLocation = createCustomer();
        this.productLocation = createProduct();
        this.dummyProductJson = PojoRepository.getProductAsJson(dummyProduct);
    }

    @After
    public void destroy() throws Exception {
        if (customerLocation != null) requestDELETEAdminRole(customerLocation);
    }

    private URI createCustomer() {
        URI customersLocation = getURI("customers");
        Customer dummyCustomer = PojoRepository.getCustomerWithRandomEmail();
        this.credentials = BasicEncoder.toAuthorizationHeaderValue(dummyCustomer.getEmail(), dummyCustomer.getPassword());
        return requestPOST(null, customersLocation, dummyCustomer).getLocation();
    }

    private URI createProduct() {
        this.dummyProduct = PojoRepository.getProduct();
        this.productsLocation = getURI("products");
        return requestPOSTWithAdminRole(productsLocation, dummyProduct).getLocation();
    }

    @Test
    public void successfulCreate() throws Exception {
        //  GET
        Response response = requestGET(null, productLocation, APPLICATION_JSON);
        JsonObject productJson = response.readEntity(JsonObject.class);
        Product product = PojoRepository.getJsonAsProduct(productJson);
        response.close();
        //  test
        String actualName = product.getName();
        double actualPrice = product.getPrice();
        String expectedName = this.dummyProduct.getName();
        double expectedCost = this.dummyProduct.getPrice();
        assertThat(actualName, is(expectedName));
        assertThat(actualPrice, is(expectedCost));
    }

    @Test
    public void rejectsCreate_notAuthorized() throws Exception {
        Response response = client.target(productsLocation).request()
                .post(Entity.json(dummyProductJson));
        response.close();
        assertThat(response.getStatusInfo(), is(UNAUTHORIZED));
    }

    @Test
    public void rejectsCreate_authAsUser() throws Exception {
        //  try create / POST
        Response response = getInvocationBuilderWithAuthHeader(credentials, productsLocation)
                .post(Entity.json(dummyProductJson));
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void conditionalRequests() throws Exception {
        //  GET
        Response response = requestGET(null, productLocation, APPLICATION_JSON);
        response.close();
        EntityTag eTag = response.getEntityTag();
        assertNotNull(eTag);
        //  send ETag with conditional GET
        response = client.target(productLocation).request()
                .header(IF_NONE_MATCH, eTag).get();
        response.close();
        assertThat(response.getStatusInfo(), is(NOT_MODIFIED));
    }
}
