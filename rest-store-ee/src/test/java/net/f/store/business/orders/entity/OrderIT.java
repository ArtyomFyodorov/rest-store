package net.f.store.business.orders.entity;

import net.f.store.business.AbstractValidatorIT;
import net.f.store.business.TestDataProducer;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.validation.ConstraintViolation;
import java.util.Collections;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class OrderIT extends AbstractValidatorIT {

    @Test
    public void validateORMapping() throws Exception {
        EntityManager em = Persistence.createEntityManagerFactory("it").createEntityManager();
        EntityTransaction tx = em.getTransaction();
        //
        tx.begin();
        em.merge(TestDataProducer.getOrder());
        tx.commit();
    }

    @Test
    public void lineItemIsEmpty() throws Exception {
        Order in = new Order();
        in.setLineItems(Collections.emptyList());

        Set<ConstraintViolation<Order>> result = this.vut.validate(in);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void lineItemIsNull() throws Exception {
        Order in = new Order();
        Set<ConstraintViolation<Order>> result = this.vut.validate(in);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

}