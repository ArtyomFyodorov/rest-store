package net.f.store.business.logging.boundary;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * The {@code LoggingInterceptor} intercepts methods that are marked
 * with the {@link net.f.store.business.logging.boundary.Loggable}
 * annotation and logs their context information.
 *
 * @author Artyom Fyodorov
 */
@Loggable
@Interceptor
public class LoggingInterceptor {
    private @Inject Logger tracer;

    @AroundInvoke
    public Object expose(InvocationContext ic) throws Exception {
        Object[] paramValues = ic.getParameters();
        this.tracer.info(">>> " + ic.getMethod() + " -> " + Arrays.toString(paramValues));
        try {
            return ic.proceed();
        } finally {
            this.tracer.info("<<< " + ic.getMethod() + " -> " + Arrays.toString(paramValues));
        }
    }

}
