# REST-Store
***
* Технологии : _Java EE 7 (JPA 2.1, CDI 1.1, Bean Validation 1.1, EJB Lite 3.2, JAX-RS 2.0)_
* Серверы приложений : _WildFly 8.x/9.x/10.x/11.x/12.x_

[Скачать код с Bitbucket](https://bitbucket.org/ArtyomFyodorov/rest-store/downloads/)

## Обзор
***
_[RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) API_ гипотетического интернет-магазина, позволяет выполнять _[CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)-операции_ над ресурсами.

Проект реализует: 

- Гипермедиа ([HATEOAS](https://en.wikipedia.org/wiki/HATEOAS))
- Пагинацию (Pagination)
- Кэширование HTTP ([HTTP/1.1 Cache-Control](https://tools.ietf.org/html/rfc7234#section-5.2))
- Аутентификацию ([Basic Auth](https://en.wikipedia.org/wiki/Basic_access_authentication)) и [Авторизацию](https://en.wikipedia.org/wiki/Authorization).

##### Цель создания примера

~~Reinvent The Wheel.~~ Применить часть спецификаций платформы _[Java EE 7](https://javaee.github.io/)_, без использования сторонних фреймворков или зависимостей.

## Компиляция и упаковка
***
Будучи проектом _[Maven](http://maven.apache.org/)_, вы можете собрать и упаковать его без выполнения тестов: `mvn clean package -Dmaven.test.skip=true` или `mvn clean install -Dmaven.test.skip=true`. После, полученный war файл можно развернуть на сервере приложений.

### Структура пакетов

![Alt text](https://bytebucket.org/ArtyomFyodorov/rest-store/raw/803c7d965ae791755f5dd9cdaf26245a688686ed/images/package-structure.png)

## Развертывание
***
+ ### WildFly с использованием jboss-cli
    * Запуск: `./standalone.sh`
    * Развертывание приложения в неинтерактивном режиме: `./jboss-cli.sh --connect --command="deploy target/rest-store.war --force"`
    * Проверка состояния: `./jboss-cli.sh --connect --command=deployment-info`
    * Извлечение: `./jboss-cli.sh --connect  --command="undeploy rest-store.war"`

### Выполнение примера с помощью cURL
После развертывания перейдите по следующему URL-адресу, выполнив консольную команду _[cURL](http://curl.haxx.se/)_:

```text
curl --head http://localhost:8080/rest-store/resources
```

Для работы с некоторыми ресурсами требуется аутентификация и авторизация запроса:

```text
administrator: curl -v --user duke@store.com:Java20 -X POST -H "Content-Type: application/json" -d "{ "name": "sunglasses", "price": 9.99, "available": 100 }" http://localhost:8080/rest-store/resources/products
customer     : curl -v --user someone@nowhere.com:Strong! -X PUT -H "Content-Type: application/json" -d "{ "product-code": 5, "quantity": 1 }" http://localhost:8080/rest-store/resources/cart
```

### Выполнение примера в окне браузера ([Swagger UI](https://swagger.io/swagger-ui/))
После развертывания перейдите по следующему URL-адресу:

```text
http://localhost:8080/rest-store/
```
Аутентификация и авторизация:

* __Administrator__
```text
login:    duke@store.com
password: Java20
```   
* __User__
```text
login:    someone@nowhere.com
password: Strong!
```


## Пример представления
[JSON Siren](https://bitbucket.org/ArtyomFyodorov/rest-store/src/803c7d965ae791755f5dd9cdaf26245a688686ed/SIREN.md)

## Лицензирование
***
[![Creative Commons License](https://i.creativecommons.org/l/by/3.0/88x31.png)](http://creativecommons.org/licenses/by/3.0/deed.ru)

This work is licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.ru).