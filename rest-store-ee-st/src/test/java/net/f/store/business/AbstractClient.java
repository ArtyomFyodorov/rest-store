package net.f.store.business;

import net.f.store.business.cart.LineItemDTO;
import net.f.store.business.customers.Customer;
import net.f.store.business.products.Product;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public abstract class AbstractClient {
    private static final String AUTH_HEADER_VALUE_ADMINISTRATOR = "Basic ZHVrZUBzdG9yZS5jb206SmF2YTIw";
    private static final String STORE_URI = "http://localhost:8080/rest-store/resources";
    //    private static final String STORE_URI = "http://reststore-depository.rhcloud.com/rest-store/resources/store";
    protected static Client client;

    @BeforeClass
    public static void initClient() throws Exception {
        client = ClientBuilder.newBuilder().build();

    }

    @AfterClass
    public static void closeClient() throws Exception {
        client.close();

    }

    //  ============================================================================================================

    protected Response requestGET(String credentials, URI location, String mediaType) {
        Response response = getInvocationBuilderWithAuthHeader(credentials, location)
                .accept(mediaType).get();

        assertThat(response.getStatusInfo(), is(OK));

        return response;
    }

    protected Response requestGETWithAdminRole(URI location, String mediaType) {
        return requestGET(AUTH_HEADER_VALUE_ADMINISTRATOR, location, mediaType);
    }

    protected Response requestPOST(String credentials, URI location, Customer customer) {
        Response response = getInvocationBuilderWithAuthHeader(credentials, location)
                .post(Entity.entity(PojoRepository.getCustomerAsJson(customer), MediaType.APPLICATION_JSON));
        response.close();
        assertThat(response.getStatusInfo(), is(CREATED));

        return response;
    }

    private Response requestPOST(String credentials, URI location, Product product) {
        Response response = getInvocationBuilderWithAuthHeader(credentials, location)
                .post(Entity.entity(PojoRepository.getProductAsJson(product), MediaType.APPLICATION_JSON));
        response.close();
        assertThat(response.getStatusInfo(), is(CREATED));

        return response;
    }

    protected Response requestPOST(String credentials, URI location, LineItemDTO lineItem) {
        Response response = getInvocationBuilderWithAuthHeader(credentials, location)
                .post(Entity.entity(PojoRepository.getLineItemAsJson(lineItem), MediaType.APPLICATION_JSON));
        response.close();
        assertThat(response.getStatusInfo(), is(CREATED));

        return response;
    }

    protected Response requestPOSTWithAdminRole(URI location, Product product) {
        return requestPOST(AUTH_HEADER_VALUE_ADMINISTRATOR, location, product);
    }

    protected Response requestPUT(String credentials, URI location, Customer customer) {
        final Response response = getInvocationBuilderWithAuthHeader(credentials, location)
                .put(Entity.entity(PojoRepository.getCustomerAsJson(customer), MediaType.APPLICATION_JSON));
        response.close();
        assertThat(response.getStatusInfo(), is(NO_CONTENT));

        return response;
    }

    public Response requestPUTWithAdminRole(URI location, Customer customer) {
        return requestPUT(AUTH_HEADER_VALUE_ADMINISTRATOR, location, customer);
    }

    public Response requestDELETEAdminRole(URI... locations) {
        Response response = null;
        for (URI location : locations) {
            assertNotNull("URI may not be null: " + location, location);
            //  cancel / DELETE
            response = getInvocationBuilderWithAuthHeaderAdminRole(location).delete();
            response.close();
            assertThat(response.getStatusInfo(), is(NO_CONTENT));
            //  check / GET
            response = getInvocationBuilderWithAuthHeaderAdminRole(location).get();
            response.close();
            assertThat(response.getStatusInfo(), is(NOT_FOUND));
        }

        return response;
    }

    //  ============================================================================================================

    protected URI getURI(String path) {
        Response response = client.target(STORE_URI).request().head();
        URI location = response.getLink(path).getUri();
        response.close();
        assertNotNull("Unable to find URI with path: " + path, location);

        return location;
    }

    protected Invocation.Builder getInvocationBuilderWithAuthHeader(String credentials, URI location) {
        return client.target(location).request().header(AUTHORIZATION, credentials);
    }

    protected Invocation.Builder getInvocationBuilderWithAuthHeaderAdminRole(URI location) {
        return getInvocationBuilderWithAuthHeader(AUTH_HEADER_VALUE_ADMINISTRATOR, location);
    }
}
