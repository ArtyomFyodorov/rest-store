package net.f.store.business.orders.boundary;

import net.f.store.business.core.boundary.PagingParams;

import javax.annotation.security.RolesAllowed;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author Artyom Fyodorov
 */
@Path("/orders")
@Produces({"application/json", "application/vnd.siren+json"})
@Consumes({"application/json", "application/vnd.siren+json"})
@RolesAllowed({"ADMIN", "USER"})
public interface OrdersResource {

    @RolesAllowed("USER")
    @POST
    Response checkout();

    @GET
    @Path("/{id:[0-9][0-9]*}")
    Response getOne(@PathParam("id") Long requestId);

    @GET
    JsonObject getAll(@BeanParam PagingParams pagingParams);
}
