package net.f.store.business.products.entity;

import net.f.store.business.AbstractValidatorIT;
import net.f.store.business.TestDataProducer;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class ProductIT extends AbstractValidatorIT {

    private Product put;

    @Before
    public void initProduct() throws Exception {
        this.put = TestDataProducer.getLineItems().get(0).getProduct();
    }

    @Test
    public void noConstraintViolation() throws Exception {
        Set<ConstraintViolation<Product>> result = this.vut.validate(this.put);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void costLowerThanSpecifiedMinimum() throws Exception {
        this.put.setPrice(.0);
        Set<ConstraintViolation<Product>> result = this.vut.validate(this.put);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void nameLowerThanSpecifiedMinimum() throws Exception {
        this.put.setName("");
        Set<ConstraintViolation<Product>> result = this.vut.validate(this.put);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void nameGreaterThanSpecifiedMaximum() throws Exception {
        String string = String.valueOf(new char[256]);
        this.put.setName(string);
        Set<ConstraintViolation<Product>> result = this.vut.validate(this.put);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void nameIsNull() throws Exception {
        this.put.setName(null);
        Set<ConstraintViolation<Product>> result = this.vut.validate(this.put);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }
}