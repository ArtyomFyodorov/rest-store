package net.f.store.business.core;

import net.f.store.business.core.entity.ErrorMessage;

import javax.persistence.OptimisticLockException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Artyom Fyodorov
 */
@Provider
public class OptimisticLockExceptionMapper implements ExceptionMapper<OptimisticLockException> {
    @Override
    public Response toResponse(OptimisticLockException ex) {
        String message = "Cannot be merged because it has changed or been deleted since it was last read. To add new data, try to send data without id";
        int statusCode = Response.Status.CONFLICT.getStatusCode();
        ErrorMessage errorMessage = ErrorMessage.of(message, statusCode);
        return Response.status(statusCode).entity(errorMessage).build();
    }
}
