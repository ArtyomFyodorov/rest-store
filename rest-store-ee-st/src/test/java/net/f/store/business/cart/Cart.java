package net.f.store.business.cart;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Artyom Fyodorov
 */
public class Cart {
    private double cartSubtotal;
    private List<LineItem> lineItems = new ArrayList<>();

    public double getSubtotal() {
        return cartSubtotal;
    }

    public void setCartSubtotal(double cartSubtotal) {
        this.cartSubtotal = cartSubtotal;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cart that = (Cart) o;
        return Double.compare(that.cartSubtotal, cartSubtotal) == 0 &&
                Objects.equals(lineItems, that.lineItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cartSubtotal, lineItems);
    }

    @Override
    public String toString() {
        return "Cart{" +
                "cartSubtotal=" + cartSubtotal +
                ", lineItems=" + lineItems +
                '}';
    }
}
