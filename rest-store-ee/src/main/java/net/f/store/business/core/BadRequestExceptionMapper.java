package net.f.store.business.core;

import net.f.store.business.core.entity.ErrorMessage;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Artyom Fyodorov
 */
@Provider
public class BadRequestExceptionMapper implements ExceptionMapper<BadRequestException> {
    @Override
    public Response toResponse(BadRequestException ex) {
        int statusCode = ex.getResponse().getStatus();
        ErrorMessage errorMessage = ErrorMessage.of(ex.getMessage(), statusCode);
        return Response.status(statusCode).entity(errorMessage).build();
    }
}
