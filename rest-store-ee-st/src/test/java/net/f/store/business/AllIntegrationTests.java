package net.f.store.business;

import net.f.store.business.cart.CartBoundaryIT;
import net.f.store.business.customers.CustomersBoundaryIT;
import net.f.store.business.orders.OrdersBoundaryIT;
import net.f.store.business.products.ProductsBoundaryIT;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Artyom Fyodorov
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        CustomersBoundaryIT.class,
        ProductsBoundaryIT.class,
        OrdersBoundaryIT.class,
        CartBoundaryIT.class
})
public class AllIntegrationTests {
}
