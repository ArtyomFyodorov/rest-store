package net.f.store.business.core.control;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Artyom Fyodorov
 */
class EMProducer {
    private @PersistenceContext(unitName = "prod") EntityManager em;

    @Produces
    public EntityManager produce() {
        return em;
    }

    public void close(@Disposes EntityManager em) {
        em.close();
    }
}
