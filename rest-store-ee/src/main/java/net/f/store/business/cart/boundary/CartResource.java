package net.f.store.business.cart.boundary;

import net.f.store.business.cart.entity.LineItemDTO;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author Artyom Fyodorov
 */
@Path("/cart")
@Produces({"application/json", "application/vnd.siren+json"})
@Consumes({"application/json", "application/vnd.siren+json"})
@RolesAllowed("USER")
public interface CartResource {

    @POST
    Response addLineItem(@NotNull @Valid LineItemDTO dto);

    @PUT
    @Path("/{id:[0-9][0-9]*}")
    Response updateLineItem(@PathParam("id") Long lineItemId, @NotNull @Valid LineItemDTO dto);

    @GET
    Response get();

    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    void deleteLineItem(@PathParam("id") Long lineItemId);
}
