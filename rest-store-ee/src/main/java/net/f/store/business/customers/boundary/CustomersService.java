package net.f.store.business.customers.boundary;

import net.f.store.business.core.boundary.Service;
import net.f.store.business.customers.entity.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

import static net.f.store.business.security.control.Digester.digest;

/**
 * @author Artyom Fyodorov
 */
@Stateless
public class CustomersService extends Service<Customer, Long> {
    private @Inject EntityManager em;

    CustomersService() {
        super(Customer.class);
    }

    @Override
    protected EntityManager getEM() {
        return this.em;
    }

    public boolean exists(String email) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Boolean> cq = cb.createQuery(Boolean.class);
        cq.from(Customer.class);
        cq.select(cb.literal(true));

        Subquery<Customer> subquery = cq.subquery(Customer.class);
        Root<Customer> subC = subquery.from(Customer.class);
        subquery.select(subC);
        subquery.where(cb.equal(subC.get(Customer_.email), email));

        cq.where(cb.exists(subquery));

        List<Boolean> result = this.em.createQuery(cq).getResultList();
        return !result.isEmpty();
    }

    public Optional<Role> findRoleByEmailAndPassword(String email, String password) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Role> cq = cb.createQuery(Role.class);
        Root<Customer> c = cq.from(Customer.class);
        cq.select(c.get(Customer_.role));

        Predicate emailPredicate = cb.equal(c.get(Customer_.email), email);
        Predicate passwordPredicate = cb.equal(c.get(Customer_.password), digest(password));
        cq.where(emailPredicate, passwordPredicate);

        TypedQuery<Role> tq = this.em.createQuery(cq);
        return uniqueResult(tq);
    }

    public Optional<Customer> findByEmail(String email) {
        return email != null ? findOne(null, email, null) : Optional.empty();
    }

    private Optional<Customer> findOne(Long id, String email, String password) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Customer> cq = cb.createQuery(Customer.class);
        Root<Customer> c = cq.from(Customer.class);
        cq.select(c);

        Predicate criteria = cb.conjunction();
        if (id != null) {
            Predicate predicate = cb.equal(c.get(Customer_.id), id);
            criteria = cb.and(predicate);
        }
        if (password != null) {
            Predicate predicate = cb.equal(c.get(Customer_.password), digest(password));
            criteria = cb.and(predicate);
        }
        if (email != null) {
            Predicate predicate = cb.equal(c.get(Customer_.email), email);
            criteria = cb.and(predicate);
        }
        cq.where(criteria);

        TypedQuery<Customer> tq = this.em.createQuery(cq);
        return uniqueResult(tq);
    }

    public List<Customer> all(int... paging) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Customer> cq = cb.createQuery(Customer.class);
        Root<Customer> c = cq.from(Customer.class);
        cq.select(c).distinct(Boolean.TRUE);

        TypedQuery<Customer> tq = this.em.createQuery(cq);
        tq.setFirstResult(paging[0]);
        tq.setMaxResults(paging[1]);
        return tq.getResultList();
    }

    public Customer save(CustomerDTO dto) {
        Customer entity = dtoToEntity(dto);
        return this.save(entity);
    }

    private Customer dtoToEntity(CustomerDTO dto) {
        Customer entity = new Customer();
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setEmail(dto.getEmail());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setPassword(dto.getPassword());

        Address address = new Address();
        address.setStreet(dto.getStreet());
        address.setCity(dto.getCity());
        address.setCountry(dto.getCountry());
        address.setState(dto.getState());
        address.setZipcode(dto.getZipcode());
        entity.setAddress(address);

        return entity;
    }

    public void update(Customer entity, CustomerDTO dto) {
        Customer customer = dtoToEntity(dto);
        customer.setId(entity.getId());
        customer.setVersion(entity.getVersion());
        customer.setRole(entity.getRole());
        this.save(customer);
    }
}
