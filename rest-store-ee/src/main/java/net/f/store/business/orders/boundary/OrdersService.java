package net.f.store.business.orders.boundary;

import net.f.store.business.cart.entity.Cart;
import net.f.store.business.cart.entity.LineItem;
import net.f.store.business.core.boundary.Service;
import net.f.store.business.customers.entity.Customer;
import net.f.store.business.customers.entity.Customer_;
import net.f.store.business.orders.entity.Order;
import net.f.store.business.orders.entity.Order_;
import net.f.store.business.products.entity.Product;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
@Stateless
public class OrdersService extends Service<Order, Long> {
    private @Inject EntityManager em;

    OrdersService() {
        super(Order.class);
    }

    @Override
    protected EntityManager getEM() {
        return this.em;
    }

    @Override
    public Optional<Order> findOne(Long id) {
        return this.findByIdAndCustomerUsername(id, null);
    }

    public Optional<Order> findOrderOfCurrentCustomer(Long id, String username) {
        return this.findByIdAndCustomerUsername(id, username);
    }

    private <T extends Serializable> Optional<Order> findByIdAndCustomerUsername(T id, String username) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Order> cq = cb.createQuery(Order.class);
        Root<Order> orderRoot = cq.from(Order.class);
        orderRoot.fetch(Order_.lineItems, JoinType.LEFT);
        cq.select(orderRoot);

        Predicate criteria = cb.conjunction();
        if (id != null) {
            Predicate orderId = cb.equal(orderRoot.get(Order_.id), id);
            criteria = cb.and(criteria, orderId);
        }
        if (username != null) {
            Path<Customer> c = orderRoot.get(Order_.customer);
            Predicate customerEmail = cb.equal(c.get(Customer_.email), username);
            criteria = cb.and(criteria, customerEmail);
        }
        cq.where(criteria);

        TypedQuery<Order> tq = this.em.createQuery(cq);
        return uniqueResult(tq);
    }

    public List<Order> allByCustomerUsername(int... paging) {
        return this.allByCustomerUsername(null, paging);
    }

    public List<Order> allOrdersOfTheCurrentCustomer(String username, int... paging) {
        return this.allByCustomerUsername(username, paging);
    }

    private List<Order> allByCustomerUsername(String username, int... paging) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Order> cq = cb.createQuery(Order.class);
        Root<Order> o = cq.from(Order.class);
        cq.select(o).distinct(Boolean.TRUE);

        if (username != null) {
            o.fetch(Order_.lineItems, JoinType.LEFT);
            Path<Customer> c = o.get(Order_.customer);
            Predicate customerEmail = cb.equal(c.get(Customer_.email), username);
            cq.where(customerEmail);
        }

        TypedQuery<Order> tq = this.em.createQuery(cq);
        tq.setFirstResult(paging[0]);
        tq.setMaxResults(paging[1]);
        return tq.getResultList();
    }

    public Order makeOrder(Customer customer) {
        Cart cart = customer.getCart();
        if (cart == null) {
            return null;
        }
        List<LineItem> lineItems = cart.getLineItems();
        lineItems.forEach(lineItem -> {
            int quantity = lineItem.getQuantity();
            Product product = lineItem.getProduct();
            int availableQuantity = product.getAvailableQuantity();
            int remainingQuantity = availableQuantity - quantity;
            product.setAvailableQuantity(remainingQuantity);
        });

        Order order = new Order();
        order.setLineItems(lineItems);
        order.setCustomer(customer);
        order.setGrandTotal(cart.getSubtotal());

        Order newOrder = this.save(order);
        customer.setCart(null);
        getEM().remove(getEM().merge(cart));

        return newOrder;
    }
}
