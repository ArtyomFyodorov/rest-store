package net.f.store.business.customers.boundary;

import net.f.store.business.core.ConflictException;
import net.f.store.business.core.boundary.JsonBuilder;
import net.f.store.business.core.boundary.PagingParams;
import net.f.store.business.customers.entity.Customer;
import net.f.store.business.customers.entity.CustomerDTO;
import net.f.store.business.customers.entity.Role;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * @author Artyom Fyodorov
 */
public class CustomersResourceBean implements CustomersResource {
    private static final String NOT_FOUND_EXCEPTION_MESSAGE = "Could not get resource for path parameter: %d";
    private static final String FORBIDDEN_EXCEPTION_MESSAGE = "You don't have the permission to access the requested resource.";
    private static final String CONFLICT_EXCEPTION_MESSAGE = "Email address already exists. Try another?";
    private static final String BAD_REQUEST_EXCEPTION_MESSAGE = "Bad Parameters: %nstart %d %nsize %d";

    @Context UriInfo info;
    @Context Request request;
    @Context SecurityContext securityContext;
    @Inject JsonBuilder jsonBuilder;
    @Inject CustomersService customersService;

    public Response save(CustomerDTO dto) {
        if (customersService.exists(dto.getEmail())) {
            throw new ConflictException(CONFLICT_EXCEPTION_MESSAGE);
        }
        Customer newCustomer = customersService.save(dto);
        String customerId = newCustomer.getId().toString();
        URI uri = info.getAbsolutePathBuilder().path(customerId).build();

        return Response.created(uri).build();
    }

    public Response getOne(Long customerId) {
        Customer customer = getCustomer(customerId);

        String customerVersion = valueOf(customer.getVersion());
        EntityTag eTag = new EntityTag(customerVersion);
        Response.ResponseBuilder rb = request.evaluatePreconditions(eTag);
        if (rb == null) {
            JsonObject customerJson = jsonBuilder.buildCustomer(customer, info);
            rb = Response.ok(customerJson).tag(eTag);
        }
        CacheControl cc = new CacheControl();
        cc.setMaxAge(86_400); // 24 h (hour)

        return rb.cacheControl(cc).build();
    }

    @Override
    public Response getCurrentCustomer() {
        String userPrincipalName = securityContext.getUserPrincipal().getName();
        Customer customer = customersService.findByEmail(userPrincipalName)
                .orElseThrow(() -> new ServerErrorException(INTERNAL_SERVER_ERROR));

        String customerVersion = valueOf(customer.getVersion());
        EntityTag eTag = new EntityTag(customerVersion);
        Response.ResponseBuilder rb = request.evaluatePreconditions(eTag);
        if (rb == null) {
            JsonObject customerJson = jsonBuilder.buildCustomer(customer, info);
            rb = Response.ok(customerJson).tag(eTag);
        }
        CacheControl cc = new CacheControl();
        cc.setMaxAge(86_400); // 24 h (hour)

        return rb.cacheControl(cc).build();
    }

    public JsonObject getAll(PagingParams pagingParams) {
        if (pagingParams.start > 0 && pagingParams.size < 1) {
            throw new BadRequestException(
                    format(BAD_REQUEST_EXCEPTION_MESSAGE, pagingParams.start, pagingParams.size));
        }
        List<Customer> customerList = customersService.all(pagingParams.start, pagingParams.size);
        if (customerList == null || customerList.isEmpty()) {
            return null; //  204 No Content
        } else {
            return jsonBuilder.buildCustomers(customerList, pagingParams, info);
        }
    }

    public Response update(Long customerId, CustomerDTO dto) {
        Customer updateableCustomer = getCustomer(customerId);
        if (!isPermittedToUpdate(updateableCustomer)) {
            throw new ForbiddenException(FORBIDDEN_EXCEPTION_MESSAGE);
        }
        String customerVersion = valueOf(updateableCustomer.getVersion());
        EntityTag eTag = new EntityTag(customerVersion);
        Response.ResponseBuilder rb = request.evaluatePreconditions(eTag);
        if (rb == null) {
            customersService.update(updateableCustomer, dto);
            rb = Response.noContent().tag(eTag);
        }

        return rb.build();
    }

    public void delete(Long customerId) {
        Customer deletableCustomer = getCustomer(customerId);
        if (!isPermittedToDelete(deletableCustomer)) {
            throw new ForbiddenException(FORBIDDEN_EXCEPTION_MESSAGE);
        }
        customersService.delete(deletableCustomer);
    }

    private Customer getCustomer(Long customerId) {
        return customersService.findOne(customerId)
                .orElseThrow(() -> new NotFoundException(format(NOT_FOUND_EXCEPTION_MESSAGE, customerId)));
    }

    private boolean isPermittedToUpdate(Customer updateableCustomer) {
        String userPrincipalName = securityContext.getUserPrincipal().getName();
        boolean isInAdminRole = securityContext.isUserInRole(Role.ADMIN.toString());
        boolean isSameCustomer = updateableCustomer.getEmail().equals(userPrincipalName);
        /*
        1. The user with the ADMIN role can update users of other roles.
        2. The user can update only his account.
         */
        return isInAdminRole || isSameCustomer;
    }

    private boolean isPermittedToDelete(Customer deletableCustomer) {
        String userPrincipalName = securityContext.getUserPrincipal().getName();
        boolean isInAdminRole = securityContext.isUserInRole(Role.ADMIN.toString());
        boolean isSameCustomer = deletableCustomer.getEmail().equals(userPrincipalName);
        /*
        1. The user with the ADMIN role cannot be deleted.
        2. The user with the ADMIN role can delete users of other roles.
        3. The user can delete only his account.
         */
        return Role.ADMIN != deletableCustomer.getRole() && (isInAdminRole || isSameCustomer);
    }
}
