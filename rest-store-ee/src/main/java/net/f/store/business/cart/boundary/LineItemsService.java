package net.f.store.business.cart.boundary;

import net.f.store.business.cart.entity.LineItem;
import net.f.store.business.core.boundary.Service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * @author Artyom Fyodorov
 */
@Stateless
public class LineItemsService extends Service<LineItem, Long> {
    private @Inject EntityManager em;

    LineItemsService() {
        super(LineItem.class);
    }

    public void update(LineItem lineItem) {
        this.save(lineItem);
    }

    @Override
    protected EntityManager getEM() {
        return this.em;
    }
}
