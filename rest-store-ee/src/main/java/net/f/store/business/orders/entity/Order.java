package net.f.store.business.orders.entity;

import net.f.store.business.cart.entity.LineItem;
import net.f.store.business.core.entity.AbstractEntity;
import net.f.store.business.customers.entity.Customer;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "orders")
@Access(AccessType.PROPERTY)
public class Order extends AbstractEntity {
    private Date creationDate = new Date();
    private Customer customer;
    private List<LineItem> lineItems = new ArrayList<>();
    private double grandTotal;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date", updatable = false)
    public Date getCreationDate() {
        return new Date(creationDate.getTime());
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = new Date(creationDate.getTime());
    }

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, optional = false)
    @JoinColumn(name = "customer_fk")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Valid
    @NotNull(message = "{net.f.constraints.order.lineItems.NotNull.message}")
    @Size(min = 1, message = "{net.f.constraints.order.lineItems.Size.message}")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "jng_order_line_item",
            joinColumns = @JoinColumn(name = "order_fk"),
            inverseJoinColumns = @JoinColumn(name = "line_item_fk"))
    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItemList) {
        this.lineItems = lineItemList;
    }

    @Column(name = "grand_total")
    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    @Override
    public String toString() {
        return "Order{" +
                super.toString() +
                ", creationDate=" + creationDate +
                ", customer=" + customer +
                ", lineItems=" + lineItems +
                ", grandTotal=" + grandTotal +
                '}';
    }
}
