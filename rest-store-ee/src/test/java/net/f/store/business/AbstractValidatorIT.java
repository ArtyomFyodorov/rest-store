package net.f.store.business;

import net.f.store.business.core.entity.AbstractEntity;
import org.junit.After;
import org.junit.Before;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * @author Artyom Fyodorov
 */
public abstract class AbstractValidatorIT {

    protected Validator vut;
    private ValidatorFactory vf;

    @Before
    public void initValidator() throws Exception {
        this.vf = Validation.buildDefaultValidatorFactory();
        this.vut = this.vf.getValidator();

    }

    @After
    public void closeVFactory() throws Exception {
        if (this.vf != null) this.vf.close();

    }

    protected <T extends AbstractEntity> void displayConstraintViolations(Set<ConstraintViolation<T>> violations) {
        System.out.printf("The number of violations: %d%n", violations.size());
        violations.forEach(cv ->
                System.out.printf(" %s.%s%n  - Invalid value: [%s]%n  - Error message: %s%n",
                        cv.getRootBeanClass().getSimpleName(),
                        cv.getPropertyPath(),
                        cv.getInvalidValue(),
                        cv.getMessage()));
    }
}
