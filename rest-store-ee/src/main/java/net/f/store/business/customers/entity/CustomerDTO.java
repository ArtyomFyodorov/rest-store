package net.f.store.business.customers.entity;

import net.f.store.business.customers.entity.constraints.Email;
import net.f.store.business.customers.entity.constraints.Password;
import net.f.store.business.customers.entity.constraints.Phone;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Artyom Fyodorov
 */
@XmlRootElement(name = "customer")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class CustomerDTO {

    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String password;
    private String street;
    private String city;
    private String state;
    private String zipcode;
    private String country;

    @NotNull(message = "{net.f.constraints.customer.firstname.NotNull.message}")
    @Size(min = 1, max = 64, message = "{net.f.constraints.customer.firstname.Size.message}")
    @XmlElement(name = "first-name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Size(min = 1, max = 64, message = "{net.f.constraints.customer.lastname.Size.message}")
    @XmlElement(name = "last-name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Phone
    @XmlElement(name = "phone-number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Password
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @NotNull(message = "{net.f.constraints.address.street.NotNull.message}")
    @Size(min = 1, max = 100, message = "{net.f.constraints.address.street.Size.message}")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @NotNull(message = "{net.f.constraints.address.city.NotNull.message}")
    @Size(min = 1, max = 100, message = "{net.f.constraints.address.city.Size.message}")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Size(min = 1, max = 100, message = "{net.f.constraints.address.state.Size.message}")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @NotNull(message = "{net.f.constraints.address.zipcode.NotNull.message}")
    @Size(min = 1, max = 30, message = "{net.f.constraints.address.zipcode.Size.message}")
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @NotNull(message = "{net.f.constraints.address.country.NotNull.message}")
    @Size(min = 1, max = 100, message = "{net.f.constraints.address.country.Size.message}")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
