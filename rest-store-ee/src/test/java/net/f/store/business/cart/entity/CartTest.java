package net.f.store.business.cart.entity;

import net.f.store.business.TestDataProducer;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class CartTest {

    @Test
    public void successfulCalculation() throws Exception {
        Cart cart = TestDataProducer.getCartWithItems();
        cart.calculateSubtotal();

        double expectedPrice = 68.48;
        double subtotal = cart.getSubtotal();

        assertThat(subtotal, is(expectedPrice));
    }
}