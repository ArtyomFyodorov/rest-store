#Siren Hypermedia
The media type for JSON Siren is `application/vnd.siren+json`.

#Examples

`GET /`
```json
{
  "links": [
    { "rel": [ "customers" ],  "href": "http://localhost:8080/rest-store/resources/customers" },
    { "rel": [ "products" ], "href": "http://localhost:8080/rest-store/resources/products" },
    { "rel": [ "orders" ], "href": "http://localhost:8080/rest-store/resources/orders" },
    { "rel": [ "cart" ], "href": "http://localhost:8080/rest-store/resources/cart" }
  ]
}
```
`GET /customers`
```json
{
  "class": [ "customers" ],
  "entities": [
    {
      "class": [ "customer" ],
      "rel": [ "item" ],
      "properties": {
        "first-name": "Duchess",
        "last-name": "Mock",
        "email": "duchess@store.com",
        "phone-number": "+7 1234567890"
      },
      "links": [
        { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/customers/2" }
      ]
    },
    { "_comment": "another customer" }
  ],
  "links": [
    { "rel": [ "next" ], "href": "http://localhost:8080/rest-store/resources/customers?start=4&size=2" },
    { "rel": [ "previous" ], "href": "http://localhost:8080/rest-store/resources/customers?start=0&size=2" },
    { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/customers" }
  ]
}
```
`GET /customers/me`
```json
{
  "class": [ "customer" ],
  "properties": {
    "first-name": "Duke",
    "last-name": "Mascot",
    "email": "duke@store.com",
    "phone-number": "+7 9876543210",
    "address": {
      "street": "class",
      "city": "package",
      "state": "component",
      "country": "tier",
      "zipcode": "layer"
    },
    "entities": [
      {
        "class": [ "cart" ],
        "rel": [ "item" ],
        "href": "http://localhost:8080/rest-store/resources/cart"
      },
      {
        "class": [ "order" ],
        "rel": [ "item" ],
        "href": "http://localhost:8080/rest-store/resources/orders"
      }
    ]
  },
  "actions": [
    {
      "name": "update-customer",
      "title": "Update customer basic info",
      "method": "PUT",
      "href": "http://localhost:8080/rest-store/resources/customers/1",
      "type": "application/json",
      "fields": [
        { "name": "first-name", "type": "text" },
        { "name": "last-name", "type": "text" },
        { "name": "email", "type": "text" },
        { "name": "phone-number", "type": "text" },
        { "name": "password", "type": "text" },
        { "name": "street", "type": "text" },
        { "name": "city", "type": "text" },
        { "name": "country", "type": "text" },
        { "name": "state", "type": "text" },
        { "name": "zipcode", "type": "text" }
      ]
    },
    {
      "name": "delete-customer",
      "title": "Delete customer data",
      "method": "DELETE",
      "href": "http://localhost:8080/rest-store/resources/customers/1"
    }
  ],
  "links": [
    { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/customers/1" }
  ]
}
```
`POST|PUT /customers` _with body:_
```json
{
  "first-name": "John",
  "last-name": "Doe",
  "email": "updated@email.com",
  "password": "Qwerty123",
  "phone-number": "+1 6579825140",
  "street": "Green",
  "city": "Enterprise",
  "state": "Java",
  "country": "World",
  "zipcode": "010101"
}
```
`GET /products`
```json
{
  "class": [ "products" ],
  "entities": [
    {
      "class": [ "product" ],
      "rel": [ "item" ],
      "properties": {
        "name": "socks",
        "price": 1.22
      },
      "links": [
        { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/products/4" }
      ]
    },
    { "_comment": "another product" }
  ],
  "actions": [
    {
      "name": "find-by-name",
      "title": "Find product by name",
      "method": "GET",
      "href": "http://localhost:8080/rest-store/resources/customers?name={name}",
      "type": "application/json",
      "fields": [
        { "name": "name", "type": "text" }
      ]
    }
  ],
  "links": [
    { "rel": [ "next" ], "href": "http://localhost:8080/rest-store/resources/products?start=4&size=2" },
    { "rel": [ "previous" ], "href": "http://localhost:8080/rest-store/resources/products?start=0&size=2" },
    { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/products"}
  ]
}
```
`POST /products` _with body:_
```json
{
  "name": "sunglasses",
  "price": 9.99,
  "available": 42
}
```
`GET /products/5`
```json
{
  "class": [ "product" ],
  "properties": {
    "product-code": 5,
    "name": "shirt",
    "price": 3.1,
    "availability": "In Stock"
  },
  "_comment": "only available if availability = In Stock",
  "actions": [
    {
      "name": "add-to-cart",
      "title": "Add to shopping cart",
      "method": "POST",
      "href": "http://localhost:8080/rest-store/resources/cart",
      "type": "application/json",
      "fields": [
        { "name": "product-code", "type": "number" },
        { "name": "quantity", "type": "number" }
      ]
    }
  ],
  "links": [
    { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/products/5" }
  ]
}
```
`POST /cart` _with body:_
```json
{
  "product-code":"5",
  "quantity":"20"
}
```
`GET /cart`
```json
{
  "class": [ "cart" ],
  "properties": {
    "cart-subtotal": 16.19
  },
  "entities": [
    {
      "class": [ "line-item" ],
      "rel": [ "item" ],
      "properties": {
        "quantity": 2,
        "entities": [
          {
            "class": [ "product" ],
            "rel": [ "item" ],
            "properties": {
              "name": "shirt",
              "price": 3.1
            },
            "links": [
              { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/products/5" }
            ]
          }
        ]
      },
      "actions": [
        {
          "name": "update-quantity",
          "title": "Change the quantity",
          "method": "PUT",
          "href": "http://localhost:8080/rest-store/resources/cart/113",
          "type": "application/json",
          "fields": [
            { "name": "quantity", "type": "number" }
          ]
        },
        {
          "name": "delete-item",
          "title": "Remove item from shopping cart",
          "method": "DELETE",
          "href": "http://localhost:8080/rest-store/resources/cart/113"
        }
      ],
      "links": [
        { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/cart/113" }
      ]
    },
    { "_comment": "another item" }
  ],
  "actions": [
    {
      "name": "checkout",
      "title": "Checkout cart",
      "method": "POST",
      "href": "http://localhost:8080/rest-store/resources/orders"
    }
  ],
  "links": [
    { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/cart" }
  ]
}
```
`PUT /cart` _with body:_
```json
{ "quantity": "5" }
```
`GET /orders`
```json
{
  "class": [ "orders" ],
  "entities": [
    {
      "class": [ "order" ],
      "rel": [ "item" ],
      "properties": {
        "creation-date": "2017-07-09 20:45:11.486",
        "grand-total": 31
      },
      "links": [
        { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/orders/105" }
      ]
    },
    { "_comment": "another order" }
  ],
  "links": [
    { "rel": [ "next" ], "href": "http://localhost:8080/rest-store/resources/orders?start=4&size=2" },
    { "rel": [ "previous" ], "href": "http://localhost:8080/rest-store/resources/orders?start=0&size=2" },
    { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/orders" }
  ]
}
```
`GET /orders/105`
```json
{
  "class": [ "order" ],
  "properties": {
    "creation-date": "2017-07-09 20:45:11.486",
    "grand-total": 40.99
  },
  "entities": [
    {
      "class": [ "line-item" ],
      "rel": [ "item" ],
      "properties": {
        "quantity": 10,
        "entities": [
          {
            "class": [ "product" ],
            "rel": [ "item" ],
            "properties": {
              "name": "shirt",
              "price": 3.1
            },
            "links": [
              { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/products/5" }
            ]
          }
        ]
      }
    },
    {"_comment": "another item"},
    {
      "class": [ "customer" ],
      "rel": [ "item" ],
      "properties": {
        "first-name": "John",
        "last-name": "Doe",
        "email": "someone@nowhere.com",
        "phone-number": "+7 9182736450"
      },
      "links": [
        { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/customers/3" }
      ]
    }
  ],
  "links": [
    { "rel": [ "self" ], "href": "http://localhost:8080/rest-store/resources/orders/105" }
  ]
}
```