package net.f.store.business.customers.entity.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Artyom Fyodorov
 */
//  +x xxxxxxxxxx
@Pattern(regexp = "^\\+(\\d{1,2})?[- ](\\d{3,14})$", message = "{net.f.constraints.phone.Pattern.message}")
@NotNull(message = "{net.f.constraints.phone.NotNull.message}")
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {})
public @interface Phone {
    String message() default "{net.f.constraints.phone.Pattern.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
