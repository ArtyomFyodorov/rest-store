package net.f.store.business.cart;

/**
 * @author Artyom Fyodorov
 */
public class LineItemDTO {
    private int quantity;
    private Long productId;

    public LineItemDTO() {
    }

    private LineItemDTO(int quantity, Long productId) {
        this.quantity = quantity;
        this.productId = productId;
    }

    public static LineItemDTO newInstance(int quantity, Long productId) {
        return new LineItemDTO(quantity, productId);
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}
