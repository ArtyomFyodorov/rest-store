package net.f.store.business;

import net.f.store.business.cart.entity.CartIT;
import net.f.store.business.cart.entity.CartTest;
import net.f.store.business.core.entity.AbstractEntityIT;
import net.f.store.business.customers.entity.AddressIT;
import net.f.store.business.customers.entity.CustomerIT;
import net.f.store.business.orders.entity.LineItemIT;
import net.f.store.business.orders.entity.OrderIT;
import net.f.store.business.products.entity.ProductIT;
import net.f.store.business.security.control.DecoderTest;
import net.f.store.business.security.control.DigesterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Artyom Fyodorov
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        CartTest.class,
        CartIT.class,
        OrderIT.class,
        CustomerIT.class,
        ProductIT.class,
        AddressIT.class,
        LineItemIT.class,
        DecoderTest.class,
        DigesterTest.class,
        AbstractEntityIT.class
})
public class AllTests {
}
