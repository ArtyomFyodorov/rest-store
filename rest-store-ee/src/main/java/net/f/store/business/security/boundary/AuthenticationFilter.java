package net.f.store.business.security.boundary;

import net.f.store.business.customers.boundary.CustomersService;
import net.f.store.business.customers.entity.Role;
import net.f.store.business.security.control.Decoder;

import javax.annotation.Priority;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.security.Principal;
import java.util.logging.Logger;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

/**
 * The {@code AuthenticationFilter} performs user authentication.
 *
 * @author Artyom Fyodorov
 */
@Provider
@Dependent
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {
    private static final String AUTH_TYPE = "Basic ";

    private @Inject CustomersService customersService;
    private @Inject Logger log;

    /**
     * Compares the data obtained by pre-decoding the value of the
     * <code><a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.8">Authorization</a></code>
     * header field with the data stored in the database.
     *
     * @param context a container request filter context.
     */
    @Override
    public void filter(ContainerRequestContext context) {
        final String authHeaderValue = context.getHeaderString(AUTHORIZATION);
        // Basic authentication scheme
        if (authHeaderValue != null && authHeaderValue.startsWith(AUTH_TYPE)) {
            final String[] credentials = Decoder.decode(authHeaderValue);
            handleBasicAuthentication(context, credentials);
        }

        // Other authentication schemes could be supported, such as 'Bearer '

    }

    private void handleBasicAuthentication(ContainerRequestContext context, String[] credentials) {
        if (credentials.length != 2) return;

        final String username = credentials[0];
        final String password = credentials[1];
        customersService.findRoleByEmailAndPassword(username, password)
                .ifPresent(role -> {
                    log.info("AUTHENTICATED " + username);
                    this.setNewSecurityContext(context, username, role);
                });
    }

    private void setNewSecurityContext(ContainerRequestContext requestContext, String username, Role userRole) {
        final boolean isSecure = requestContext.getSecurityContext().isSecure();
        requestContext.setSecurityContext(new SecurityContext() {
            @Override
            public Principal getUserPrincipal() {
                return () -> username;
            }

            @Override
            public boolean isUserInRole(String role) {
                return userRole.toString().equals(role);
            }

            @Override
            public boolean isSecure() {
                return isSecure;
            }

            @Override
            public String getAuthenticationScheme() {
                return SecurityContext.BASIC_AUTH;
            }
        });
    }
}
