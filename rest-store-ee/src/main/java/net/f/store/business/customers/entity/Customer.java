package net.f.store.business.customers.entity;

import net.f.store.business.cart.entity.Cart;
import net.f.store.business.core.entity.AbstractEntity;
import net.f.store.business.customers.entity.constraints.Email;
import net.f.store.business.customers.entity.constraints.Password;
import net.f.store.business.customers.entity.constraints.Phone;
import net.f.store.business.customers.entity.converters.RoleConverter;
import net.f.store.business.orders.entity.Order;
import net.f.store.business.security.control.Digester;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Access(AccessType.PROPERTY)
@Table(name = "customers")
public class Customer extends AbstractEntity {
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String password;
    private Address address;
    private Role role;
    private List<Order> orders = new ArrayList<>();
    private Cart cart;

    //  Lifecycle Methods
    @PreUpdate
    @PrePersist
    private void digestPassword() {
        this.password = Digester.digest(this.password);
    }

    @NotNull(message = "{net.f.constraints.customer.firstname.NotNull.message}")
    @Size(min = 1, max = 64, message = "{net.f.constraints.customer.firstname.Size.message}")
    @Column(name = "first_name", length = 30, nullable = false)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Size(min = 1, max = 64, message = "{net.f.constraints.customer.lastname.Size.message}")
    @Column(name = "last_name", length = 30)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Email
    @Column(length = 100, unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Phone
    @Column(name = "phone_number", length = 14)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Valid
    @NotNull(message = "{net.f.constraints.customer.address.NotNull.message}")
    @Embedded
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Password
    @Column(nullable = false)
    @XmlTransient
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Convert(converter = RoleConverter.class)
    @Column(length = 64)
    public Role getRole() {
        return Optional.ofNullable(this.role)
                .orElse(Role.USER);
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @OneToMany(mappedBy = "customer", cascade = {CascadeType.ALL}, orphanRemoval = true, fetch = FetchType.EAGER)
    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @OneToOne(mappedBy = "customer", cascade = {CascadeType.ALL}, orphanRemoval = true)
    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    @Override
    public String toString() {
        return "Customer{" +
                super.toString() +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address=" + address +
                ", role=" + role +
                '}';
    }
}
