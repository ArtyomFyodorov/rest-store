package net.f.store.business.cart.entity;

import net.f.store.business.core.entity.AbstractEntity;
import net.f.store.business.products.entity.Product;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "line_items")
@Access(AccessType.PROPERTY)
public class LineItem extends AbstractEntity {
    private int quantity;
    private Product product;

    public LineItem() {
    }

    public LineItem(int quantity, Product product) {
        this.quantity = quantity;
        this.product = product;
    }

    @Transient
    public double getPrice() {
        BigDecimal unitPrice = valueOf(product.getPrice());
        return unitPrice.multiply(valueOf(quantity)).doubleValue();
    }

    @Min(value = 1, message = "{net.f.constraints.lineItem.quantity.Min.message}")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Valid
    @NotNull(message = "{net.f.constraints.lineItem.product.NotNull.message}")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "product_fk")
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "LineItem{" +
                super.toString() +
                ", quantity=" + quantity +
                ", product=" + product +
                '}';
    }
}
