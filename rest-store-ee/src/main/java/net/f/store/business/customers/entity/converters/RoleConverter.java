package net.f.store.business.customers.entity.converters;

import net.f.store.business.customers.entity.Role;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author Artyom Fyodorov
 */
@Converter
public class RoleConverter implements AttributeConverter<Role, String> {
    @Override
    public String convertToDatabaseColumn(Role attribute) {
        switch (attribute) {
            case USER:
                return "User";
            case ADMIN:
                return "Administrator";
            default:
                throw new IllegalArgumentException("Unknown " + attribute);
        }
    }

    @Override
    public Role convertToEntityAttribute(String dbData) {
        switch (dbData) {
            case "User":
                return Role.USER;
            case "Administrator":
                return Role.ADMIN;
            default:
                throw new IllegalArgumentException("Unknown " + dbData);
        }
    }
}
