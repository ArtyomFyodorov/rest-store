package net.f.store.business.cart;

import net.f.store.business.AbstractClient;
import net.f.store.business.BasicEncoder;
import net.f.store.business.PojoRepository;
import net.f.store.business.customers.Customer;
import net.f.store.business.products.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonObject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Response;
import java.net.URI;

import static javax.ws.rs.core.HttpHeaders.IF_MATCH;
import static javax.ws.rs.core.HttpHeaders.IF_NONE_MATCH;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class CartBoundaryIT extends AbstractClient {
    private final String jsonToUpdateLineItem = "{ \"quantity\": \"42\" }";
    private Long productId;
    private JsonObject lineItemJson;
    private String credentials;
    private URI customerLocation;
    private URI cartLocation;
    private URI itemInCartURI;

    @Before
    public void init() throws Exception {
        this.customerLocation = createCustomer();
        URI productLocation = createProduct();

        String[] uriSegments = productLocation.toString().split("/");
        this.productId = Long.parseLong(uriSegments[uriSegments.length - 1]);
        this.itemInCartURI = addProductToCart(6, productId).getLocation();
    }

    @After
    public void destroy() throws Exception {
        if (customerLocation != null) requestDELETEAdminRole(customerLocation);
    }

    private URI createCustomer() {
        URI customersLocation = getURI("customers");
        Customer dummyCustomer = PojoRepository.getCustomerWithRandomEmail();
        this.credentials = BasicEncoder.toAuthorizationHeaderValue(dummyCustomer.getEmail(), dummyCustomer.getPassword());
        return requestPOST(null, customersLocation, dummyCustomer).getLocation();
    }

    private URI createProduct() {
        Product dummyProduct = PojoRepository.getProduct();
        URI productsLocation = getURI("products");
        return requestPOSTWithAdminRole(productsLocation, dummyProduct).getLocation();
    }

    private Response addProductToCart(int quantity, long productId) {
        this.cartLocation = getURI("cart");
        LineItemDTO lineItem = LineItemDTO.newInstance(quantity, productId);
        this.lineItemJson = PojoRepository.getLineItemAsJson(lineItem);
        return requestPOST(credentials, cartLocation, lineItem);
    }

    @Test
    public void successfulAddToCart() throws Exception {
        //  GET
        Response response = requestGET(credentials, cartLocation, APPLICATION_JSON);
        JsonObject cartJson = response.readEntity(JsonObject.class);
        Cart order = PojoRepository.getJsonAsCart(cartJson);
        response.close();
        //  test
        double actualTotalPrice = order.getSubtotal();
        double expectedTotalPrice = 1499.94;  //  quantity = 6; cost = 249.99
        final double delta = 0.009;
        assertEquals(expectedTotalPrice, actualTotalPrice, delta);
    }

    @Test
    public void rejectsAddToCart_quantity_is_more_than_available() throws Exception {
        //  try create / POST
        LineItemDTO lineItem = LineItemDTO.newInstance(100500, productId);
        Response response = getInvocationBuilderWithAuthHeader(credentials, cartLocation)
                .post(Entity.entity(PojoRepository.getLineItemAsJson(lineItem), APPLICATION_JSON));
        response.close();
        assertThat(response.getStatusInfo(), is(BAD_REQUEST));
    }

    @Test
    public void rejectsAddToCart_notAuthorized() throws Exception {
        Response response = getInvocationBuilderWithAuthHeader(null, cartLocation)
                .post(Entity.json(lineItemJson));
        response.close();
        assertThat(response.getStatusInfo(), is(UNAUTHORIZED));
    }

    @Test
    public void rejectsAddToCart_authAsAdmin() throws Exception {
        //  try create / POST
        Response response = getInvocationBuilderWithAuthHeaderAdminRole(cartLocation)
                .post(Entity.json(lineItemJson));
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void itemSuccessfullyDeleted_authAsUser() throws Exception {
        //  DELETE
        Response response = getInvocationBuilderWithAuthHeader(credentials, itemInCartURI).delete();
        response.close();
        assertThat(response.getStatusInfo(), is(NO_CONTENT));
    }

    @Test
    public void rejectsDeletingAnItem_authAsAdmin() throws Exception {
        //  try remove / DELETE
        Response response = getInvocationBuilderWithAuthHeaderAdminRole(itemInCartURI).delete();
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void successfulUpdatingCart_authAsUserOwner() throws Exception {
        //  update / PUT
        Response response = getInvocationBuilderWithAuthHeader(credentials, itemInCartURI)
                .put(Entity.json(jsonToUpdateLineItem));
        assertThat(response.getStatusInfo(), is(NO_CONTENT));
        //  get after updating / GET
        response = requestGET(credentials, cartLocation, APPLICATION_JSON);
        JsonObject cartJson = response.readEntity(JsonObject.class);
        Cart cart = PojoRepository.getJsonAsCart(cartJson);
        response.close();
        //  test
        double actualTotalPrice = cart.getSubtotal();
        double expectedTotalPrice = 10499.58;  //  quantity = 42; cost = 249.99
        final double delta = 0.009;
        assertEquals(expectedTotalPrice, actualTotalPrice, delta);
    }

    @Test
    public void rejectsUpdatingCart_authAsAdmin() throws Exception {
        //  try update / PUT
        Response response = getInvocationBuilderWithAuthHeaderAdminRole(itemInCartURI)
                .put(Entity.json(jsonToUpdateLineItem));
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void rejectsGetCart_authAsAdmin() throws Exception {
        //  try get / GET
        Response response = getInvocationBuilderWithAuthHeaderAdminRole(cartLocation).get();
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void conditionalRequests() throws Exception {
        //  GET
        Response response = requestGET(credentials, cartLocation, APPLICATION_JSON);
        response.close();
        EntityTag eTag = response.getEntityTag();
        assertNotNull(eTag);
        //  send ETag with Conditional GET
        response = getInvocationBuilderWithAuthHeader(credentials, cartLocation)
                .header(IF_NONE_MATCH, eTag).get();
        response.close();
        assertThat(response.getStatusInfo(), is(NOT_MODIFIED));
        //  update and send a bad ETag with Conditional PUT
        response = getInvocationBuilderWithAuthHeader(credentials, itemInCartURI)
                .header(IF_MATCH, "CASUAL")
                .put(Entity.json(jsonToUpdateLineItem));
        response.close();
        assertThat(response.getStatusInfo(), is(PRECONDITION_FAILED));
    }
}