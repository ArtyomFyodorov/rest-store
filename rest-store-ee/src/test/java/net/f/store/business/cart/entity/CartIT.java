package net.f.store.business.cart.entity;

import net.f.store.business.AbstractValidatorIT;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.Collections;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class CartIT extends AbstractValidatorIT {

    @Test
    public void emptyLineItem() throws Exception {
        Cart cart = new Cart();
        cart.setLineItems(Collections.emptyList());

        Set<ConstraintViolation<Cart>> result = this.vut.validate(cart);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void lineItemIsNull() throws Exception {
        Cart cart = new Cart();
        Set<ConstraintViolation<Cart>> result = this.vut.validate(cart);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }
}