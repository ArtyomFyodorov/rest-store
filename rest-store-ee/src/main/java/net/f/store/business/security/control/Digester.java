package net.f.store.business.security.control;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * The {@code Digester} class that implements the {@code SHA-256} algorithm.
 *
 * @author Artyom Fyodorov
 */
public final class Digester {
    private static final String MESSAGE_DIGEST_ALGORITHM = "SHA-256";
    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    private Digester() {
    }

    /**
     * Computes a hash of a string using the {@code SHA-256} algorithm.
     *
     * @param plainText the string whose hash must be computed
     * @return a {@code Base64} encoded {@code String}
     */
    public static String digest(String plainText) {
        return digest(plainText, DEFAULT_CHARSET);
    }

    /**
     * Computes a hash of a string using the SHA-256 algorithm.
     *
     * @param plainText the string whose hash must be computed
     * @param charset   the {@linkplain java.nio.charset.Charset}
     *                  to be used to encode the {@code String}
     * @return the Base64 encoded String for the resulting hash value.
     */
    public static String digest(String plainText, Charset charset) {
        if (plainText == null || "".equals(plainText)) {
            throw new IllegalArgumentException("Invalid header");
        }
        if (charset == null) {
            throw new IllegalArgumentException("Charset is null");
        }

        try {
            MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST_ALGORITHM);
            md.update(plainText.getBytes(charset));
            byte[] hash = md.digest();

            return new String(Base64.getEncoder().encode(hash), charset);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
