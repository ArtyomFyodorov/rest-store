package net.f.store.business.orders.entity;

import net.f.store.business.AbstractValidatorIT;
import net.f.store.business.TestDataProducer;
import net.f.store.business.cart.entity.LineItem;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class LineItemIT extends AbstractValidatorIT {

    private LineItem liut;

    @Before
    public void initLineItem() throws Exception {
        this.liut = TestDataProducer.getLineItems().get(0);

    }

    @Test
    public void noConstraintViolation() throws Exception {
        Set<ConstraintViolation<LineItem>> result = this.vut.validate(this.liut);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void quantityLessThanOne() throws Exception {
        this.liut.setQuantity(0);
        Set<ConstraintViolation<LineItem>> result = this.vut.validate(this.liut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void productIsNull() throws Exception {
        this.liut.setProduct(null);
        Set<ConstraintViolation<LineItem>> result = this.vut.validate(this.liut);
//        displayConstraintViolations(result);

        int expected = 1;
        assertThat(result.size(), is(expected));
    }
}