INSERT INTO customers (version, city, country, state, street, zip_code, email, first_name, last_name, password, phone_number, role, id) VALUES (0, 'package', 'tier', 'component', 'class', 'layer', 'duke@store.com', 'Duke', 'Mascot', 'N6gbW9gzuQe4gJi52uPi/Pdys5eAyYMWxm0fSyGL+D4=', '+7 9876543210', 'Administrator', 1);
INSERT INTO customers (version, city, country, state, street, zip_code, email, first_name, last_name, password, phone_number, role, id) VALUES (0, 'package', 'tier', 'component', 'class', 'layer', 'duchess@store.com', 'Duchess', 'Mock', 'd6rhhSA+3GNXZ225XKol0POY1ALBcj5qe0LP6NKWfy4=', '+7 1234567890', 'Administrator', 2);
INSERT INTO customers (version, city, country, state, street, zip_code, email, first_name, last_name, password, phone_number, role, id) VALUES (0, 'Enterprise', 'World', 'Java', 'Green', '010101', 'someone@nowhere.com', 'Foo', 'Bar', '+drOWhzjvOsngC6j123sJ9JTeYsHERImuTIte7n5P1Q=', '+7 9182736450', 'User', 3);

INSERT INTO products (version, name, price, available, id) VALUES (0, 'socks', 1.22, 0, 4);
INSERT INTO products (version, name, price, available, id) VALUES (0, 'shirt', 3.10, 42, 5);
