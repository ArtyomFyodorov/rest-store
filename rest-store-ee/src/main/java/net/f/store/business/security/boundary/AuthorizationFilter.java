package net.f.store.business.security.boundary;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.Dependent;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * The {@code AuthorizationFilter} performs user authorization.
 *
 * @author Artyom Fyodorov
 */
@Provider
@Dependent
@Priority(Priorities.AUTHORIZATION)
public class AuthorizationFilter implements ContainerRequestFilter {
    private static final String MESSAGE_FORBIDDEN = "You don't have permission to access this resource.";
    private static final String MESSAGE_UNAUTHENTICATED = "Authentication is required to access this resource.";
    private static final String REALM = "My Realm";  // A description of the protected area.

    private @Context ResourceInfo resourceInfo;

    @Override
    public void filter(final ContainerRequestContext requestContext) {
        Method method = resourceInfo.getResourceMethod();

        // @DenyAll on the method takes precedence over @RolesAllowed and @PermitAll
        if (method.isAnnotationPresent(DenyAll.class)) {
            throw new ForbiddenException(MESSAGE_FORBIDDEN);
        }

        // @RolesAllowed on the method takes precedence over @PermitAll
        RolesAllowed rolesAllowed = method.getAnnotation(RolesAllowed.class);
        if (rolesAllowed != null) {
            performAuthorization(rolesAllowed.value(), requestContext);
            return;
        }

        // @PermitAll on the method takes precedence over @RolesAllowed on the class
        if (method.isAnnotationPresent(PermitAll.class)) {
            return; // Do nothing
        }

        // @DenyAll can't be attached to classes

        // @RolesAllowed on the class takes precedence over @PermitAll on the class
        rolesAllowed = resourceInfo.getResourceClass().getAnnotation(RolesAllowed.class);
        if (rolesAllowed != null) {
            performAuthorization(rolesAllowed.value(), requestContext);
        }

        // @PermitAll on the class
        if (resourceInfo.getResourceClass().isAnnotationPresent(PermitAll.class)) {
            return; // Do nothing
        }

        // Authentication is required for non-annotated methods
        if (isUnauthenticated(requestContext)) {
            throw new AuthenticationException(MESSAGE_UNAUTHENTICATED, REALM);
        }
    }

    /**
     * Perform authorization based on roles.
     */
    private void performAuthorization(String[] rolesAllowed, ContainerRequestContext requestContext) {
        if (rolesAllowed.length > 0 && isUnauthenticated(requestContext)) {
            throw new AuthenticationException(MESSAGE_UNAUTHENTICATED, REALM);
        }

        Arrays.stream(rolesAllowed)
                .filter(role -> requestContext.getSecurityContext().isUserInRole(role))
                .findAny().orElseThrow(() -> new ForbiddenException(MESSAGE_FORBIDDEN));

    }

    /**
     * Check if the user is unauthenticated.
     */
    private boolean isUnauthenticated(final ContainerRequestContext requestContext) {
        return requestContext.getSecurityContext().getUserPrincipal() == null;
    }
}
