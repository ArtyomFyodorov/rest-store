package net.f.store.business.cart.entity;

import net.f.store.business.core.entity.AbstractEntity;
import net.f.store.business.customers.entity.Customer;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "shopping_cart")
@Access(AccessType.PROPERTY)
public class Cart extends AbstractEntity {
    private double subtotal;
    private List<LineItem> lineItems = new ArrayList<>();
    private Customer customer;

    @PostLoad
    @PrePersist
    @PreUpdate
    void calculateSubtotal() {
        this.subtotal = lineItems.stream().mapToDouble(LineItem::getPrice).sum();
    }

    @Column(name = "cart_subtotal")
    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double cartSubtotal) {
        this.subtotal = cartSubtotal;
    }

    @Valid
    @NotNull(message = "{net.f.constraints.order.lineItems.NotNull.message}")
    @Size(min = 1, message = "{net.f.constraints.order.lineItems.Size.message}")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinTable(name = "jng_cart_line_item",
            joinColumns = @JoinColumn(name = "cart_fk"),
            inverseJoinColumns = @JoinColumn(name = "line_item_fk"))
    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public void addLineItem(@Valid @NotNull LineItem lineItem) {
        for (LineItem li : lineItems) {
            if (li.getProduct().equals(lineItem.getProduct())) {
                li.setQuantity(lineItem.getQuantity());
                return;
            }
        }
        this.lineItems.add(lineItem);
    }

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, optional = false)
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Cart{" +
                super.toString() +
                ", subtotal=" + subtotal +
                ", lineItems=" + lineItems +
                ", customer=" + customer +
                '}';
    }
}
