package net.f.store.business.security.control;

import org.junit.Test;

import static org.hamcrest.Matchers.arrayContaining;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class DecoderTest {
    private static final String AUTH_HEADER = "Basic ZHVrZUBtYXNjb3QuY29tOkphdmEyMA==";

    @Test
    public void successfulDecoding() throws Exception {
        String[] decodedData = Decoder.decode(AUTH_HEADER);
        assertThat(decodedData, arrayContaining("duke@mascot.com", "Java20"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void rejectsCharsetIsNull() throws Exception {
        Decoder.decode(AUTH_HEADER, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void rejectsHeaderIsInvalid() throws Exception {
        Decoder.decode("");
    }
}