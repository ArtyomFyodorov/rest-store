package net.f.store.business.core.boundary;

import net.f.store.business.cart.entity.Cart;
import net.f.store.business.cart.entity.LineItem;
import net.f.store.business.customers.entity.Address;
import net.f.store.business.customers.entity.Customer;
import net.f.store.business.orders.entity.Order;
import net.f.store.business.products.entity.Product;

import javax.inject.Inject;
import javax.json.*;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

import static java.lang.String.format;
import static javax.json.Json.createArrayBuilder;
import static javax.json.Json.createObjectBuilder;

/**
 * @author Artyom Fyodorov
 */
public class JsonBuilder {

    @Inject UriProducer uriProducer;

    JsonObject buildStore(UriInfo uriInfo) {

        return createObjectBuilder()
                .add("links", createArrayBuilder()
                        .add(createLinkObject("customers", uriProducer.forCustomers(uriInfo)))
                        .add(createLinkObject("products", uriProducer.forProducts(uriInfo)))
                        .add(createLinkObject("orders", uriProducer.forOrders(uriInfo)))
                        .add(createLinkObject("cart", uriProducer.forCart(uriInfo)))
                ).build();
    }

    //  ============================================================================================================

    public JsonObject buildProducts(List<Product> products, PagingParams pagingParams, UriInfo uriInfo) {
        final JsonArray productEntities = products.stream()
                .map(p -> buildProductTeaser(p, uriInfo))
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();

        JsonArrayBuilder linksArrayBuilder = createPaginationLinksArrayBuilder(
                products.size(), pagingParams, uriProducer.forProducts(uriInfo))
                        .add(createLinkObject("self", uriProducer.forProducts(uriInfo)));

        JsonArrayBuilder actionsArrayBuilder = createArrayBuilder().add(createObjectBuilder()
                .add("name", "find-by-name")
                .add("title", "Find product by name")
                .add("method", HttpMethod.GET)
                .add("href", format("%s?name={name}", uriProducer.forCustomers(uriInfo)))
                .add("type", MediaType.APPLICATION_JSON)
                .add("fields", createArrayBuilder()
                        .add(createObjectBuilder()
                                .add("name", "name")
                                .add("type", "text")
                        )
                )
        );

        return createObjectBuilder()
                .add("class", createArrayBuilder().add("products"))
                .add("entities", productEntities)
                .add("actions", actionsArrayBuilder)
                .add("links", linksArrayBuilder).build();
    }

    private JsonObject buildProductTeaser(Product product, UriInfo uriInfo) {
        return createObjectBuilder()
                .add("class", createArrayBuilder().add("product"))
                .add("rel", createArrayBuilder().add("item"))
                .add("properties", createObjectBuilder()
                        .add("name", product.getName())
                        .add("price", product.getPrice()))
                .add("links", createArrayBuilder()
                        .add(createLinkObject("self", uriProducer.forProduct(uriInfo, product))))
                .build();
    }

    public JsonObject buildProduct(Product product, UriInfo uriInfo, boolean actionsIncluded) {
        final JsonObjectBuilder productBuilder = createObjectBuilder()
                .add("class", createArrayBuilder().add("product"))
                .add("properties", createObjectBuilder()
                        .add("product-code", product.getId())
                        .add("name", product.getName())
                        .add("price", product.getPrice())
                        .add("availability", product.availability())
                );

        if (actionsIncluded) {
            productBuilder.add("actions", createArrayBuilder().add(createObjectBuilder()
                            .add("name", "add-to-cart")
                            .add("title", "Add to shopping cart")
                            .add("method", HttpMethod.POST)
                            .add("href", uriProducer.forCart(uriInfo).toString())
                            .add("type", MediaType.APPLICATION_JSON)
                            .add("fields", createArrayBuilder()
                                    .add(createObjectBuilder()
                                                    .add("name", "product-code")
                                                    .add("type", "number")
//                                    .add("required", true)
                                    ).add(createObjectBuilder()
                                                    .add("name", "quantity")
                                                    .add("type", "number")
//                                    .add("required", true)
                                    )))
            );
        }

        productBuilder.add("links", createArrayBuilder()
                .add(createLinkObject("self", uriProducer.forProduct(uriInfo, product))));

        return productBuilder.build();
    }

    //  ============================================================================================================

    private JsonObject buildOrdersMinimal(UriInfo uriInfo) {
        return createObjectBuilder()
                .add("class", createArrayBuilder().add("orders"))
                .add("rel", createArrayBuilder().add("item"))
                .add("href", uriProducer.forOrders(uriInfo).toString())
                .build();
    }

    public JsonObject buildOrders(List<Order> orders, PagingParams pagingParams, UriInfo uriInfo) {
        final JsonArray orderEntities = orders.stream()
                .map(o -> buildOrderTeaser(o, uriInfo))
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();

        JsonArrayBuilder linksArrayBuilder = createPaginationLinksArrayBuilder(
                orders.size(), pagingParams, uriProducer.forOrders(uriInfo))
                        .add(createLinkObject("self", uriProducer.forOrders(uriInfo)));

        return createObjectBuilder()
                .add("class", createArrayBuilder().add("orders"))
                .add("entities", orderEntities)
                .add("links", linksArrayBuilder)
                .build();
    }

    private JsonObject buildOrderTeaser(Order order, UriInfo uriInfo) {
        return createObjectBuilder()
                .add("class", createArrayBuilder().add("order"))
                .add("rel", createArrayBuilder().add("item"))
                .add("properties", createObjectBuilder()
                        .add("creation-date", order.getCreationDate().toString())
                        .add("grand-total", order.getGrandTotal()))
                .add("links", createArrayBuilder()
                        .add(createLinkObject("self", uriProducer.forOrder(uriInfo, order))))
                .build();
    }

    public JsonObject buildOrder(Order order, UriInfo uriInfo) {
        JsonArrayBuilder jsonArrayBuilder = order.getLineItems().stream()
                .map(li -> buildLineItem(li, uriInfo, false))
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .add(buildCustomerTeaser(order.getCustomer(), uriInfo));

        return createObjectBuilder()
                .add("class", createArrayBuilder().add("order"))
                .add("properties", createObjectBuilder()
                        .add("creation-date", order.getCreationDate().toString())
                        .add("grand-total", order.getGrandTotal()))
                .add("entities", jsonArrayBuilder)
                .add("links", createArrayBuilder()
                        .add(createLinkObject("self", uriProducer.forOrder(uriInfo, order))))
                .build();
    }

    //  ============================================================================================================

    private JsonObject buildCartMinimal(UriInfo uriInfo) {
        return createObjectBuilder()
                .add("class", createArrayBuilder().add("cart"))
                .add("rel", createArrayBuilder().add("item"))
                .add("href", uriProducer.forCart(uriInfo).toString())
                .build();

    }

    public JsonObject buildCart(Cart cart, UriInfo uriInfo) {
        if (cart == null) return null;

        JsonArray lineItemEntities = cart.getLineItems().stream()
                .map(li -> buildLineItem(li, uriInfo, true))
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add).build();

        return createObjectBuilder()
                .add("class", createArrayBuilder().add("cart"))
                .add("properties", createObjectBuilder().add("cart-subtotal", cart.getSubtotal()))
                .add("entities", lineItemEntities)
                .add("actions", createArrayBuilder().add(createObjectBuilder()
                        .add("name", "checkout")
                        .add("title", "Checkout cart")
                        .add("method", HttpMethod.POST)
                        .add("href", uriProducer.forOrders(uriInfo).toString())))
                .add("links", createArrayBuilder()
                        .add(createLinkObject("self", uriProducer.forCart(uriInfo))))
                .build();
    }

    //  ============================================================================================================

    private JsonObject buildLineItem(LineItem lineItem, UriInfo uriInfo, boolean actionsIncluded) {

        JsonObjectBuilder entityBuilder = createObjectBuilder()
                .add("class", createArrayBuilder().add("line-item"))
                .add("rel", createArrayBuilder().add("item"))
                .add("properties", createObjectBuilder()
                        .add("quantity", lineItem.getQuantity())
                        .add("entities", createArrayBuilder().add(buildProductTeaser(lineItem.getProduct(), uriInfo))));

        if (actionsIncluded) {
            entityBuilder.add("actions", createArrayBuilder()
                    .add(createObjectBuilder()
                            .add("name", "update-quantity")
                            .add("title", "Change the quantity")
                            .add("method", HttpMethod.PUT)
                            .add("href", uriProducer.forLineItem(uriInfo, lineItem).toString())
                            .add("type", MediaType.APPLICATION_JSON)
                            .add("fields", createArrayBuilder()
                                    .add(createObjectBuilder()
                                                    .add("name", "quantity")
                                                    .add("type", "number")
//                                            .add("required", true)
                                    )))
                    .add(createObjectBuilder()
                            .add("name", "delete-item")
                            .add("title", "Remove item from shopping cart")
                            .add("method", HttpMethod.DELETE)
                            .add("href", uriProducer.forLineItem(uriInfo, lineItem).toString())))
                    .add("links", createArrayBuilder().add(createLinkObject("self", uriProducer.forLineItem(uriInfo, lineItem))));

        }

        return entityBuilder.build();
    }

    //  ============================================================================================================

    public JsonObject buildCustomers(List<Customer> customers, PagingParams pagingParams, UriInfo uriInfo) {
        JsonArray customerEntities = customers.stream()
                .map(c -> buildCustomerTeaser(c, uriInfo))
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();

        JsonArrayBuilder linksArrayBuilder = createPaginationLinksArrayBuilder(
                customers.size(), pagingParams, uriProducer.forCustomers(uriInfo))
                        .add(createLinkObject("self", uriProducer.forCustomers(uriInfo)));

        return createObjectBuilder()
                .add("class", createArrayBuilder().add("customers"))
                .add("entities", customerEntities)
                .add("links", linksArrayBuilder)
                .build();
    }

    private JsonObject buildCustomerTeaser(Customer customer, UriInfo uriInfo) {
        return createObjectBuilder()
                .add("class", createArrayBuilder().add("customer"))
                .add("rel", createArrayBuilder().add("item"))
                .add("properties", createObjectBuilder()
                        .add("first-name", customer.getFirstName())
                        .add("last-name", customer.getLastName())
                        .add("email", customer.getEmail())
                        .add("phone-number", customer.getPhoneNumber()))
                .add("links", createArrayBuilder()
                        .add(createLinkObject("self", uriProducer.forCustomer(uriInfo, customer))))
                .build();
    }

    public JsonObject buildCustomer(Customer customer, UriInfo uriInfo) {
        JsonArrayBuilder entitiesBuilder = createArrayBuilder()
                .add(buildCartMinimal(uriInfo))
                .add(buildOrdersMinimal(uriInfo));

        return createObjectBuilder()
                .add("class", createArrayBuilder().add("customer"))
                .add("properties", createObjectBuilder()
                        .add("first-name", customer.getFirstName())
                        .add("last-name", customer.getLastName())
                        .add("email", customer.getEmail())
                        .add("phone-number", customer.getPhoneNumber())
                        .add("address", buildAddress(customer.getAddress()))
                        .add("entities", entitiesBuilder))
                .add("actions", createArrayBuilder()
                        .add(createObjectBuilder()
                                        .add("name", "update-customer")
                                        .add("title", "Update customer basic info")
                                        .add("method", HttpMethod.PUT)
                                        .add("href", uriProducer.forCustomer(uriInfo, customer).toString())
                                        .add("type", MediaType.APPLICATION_JSON)
                                        .add("fields", createArrayBuilder()
                                                        .add(createObjectBuilder()
                                                                        .add("name", "first-name")
                                                                        .add("type", "text")
//                                                                .add("required", true)
                                                        ).add(createObjectBuilder()
                                                                .add("name", "last-name")
                                                                .add("type", "text")
//                                                            .add("required", true)
                                                        ).add(createObjectBuilder()
                                                                .add("name", "email")
                                                                .add("type", "text")
//                                                            .add("required", true)
                                                        ).add(createObjectBuilder()
                                                                .add("name", "phone-number")
                                                                .add("type", "text")
//                                                            .add("required", true)
                                                        ).add(createObjectBuilder()
                                                                .add("name", "password")
                                                                .add("type", "text")
//                                                            .add("required", true)
                                                        ).add(createObjectBuilder()
                                                                .add("name", "street")
                                                                .add("type", "text")
//                                                            .add("required", true)
                                                        ).add(createObjectBuilder()
                                                                .add("name", "city")
                                                                .add("type", "text")
//                                                            .add("required", true)
                                                        ).add(createObjectBuilder()
                                                                .add("name", "country")
                                                                .add("type", "text")
//                                                            .add("required", true)
                                                        ).add(createObjectBuilder()
                                                                .add("name", "state")
                                                                .add("type", "text")
//                                                            .add("required", true)
                                                        ).add(createObjectBuilder()
                                                                .add("name", "zipcode")
                                                                .add("type", "text")
//                                                            .add("required", true)
                                                        )
                                        )
                        ).add(createObjectBuilder()
                                .add("name", "delete-customer")
                                .add("title", "Delete customer data")
                                .add("method", HttpMethod.DELETE)
                                .add("href", uriProducer.forCustomer(uriInfo, customer).toString())))
                .add("links", createArrayBuilder()
                        .add(createLinkObject("self", uriProducer.forCustomer(uriInfo, customer))))
                .build();
    }


    private JsonObject buildAddress(Address address) {
        return createObjectBuilder()
                .add("street", address.getStreet())
                .add("city", address.getCity())
                .add("state", address.getState())
                .add("country", address.getCountry())
                .add("zipcode", address.getZipcode())
                .build();
    }
    //  ============================================================================================================

    private JsonArrayBuilder createPaginationLinksArrayBuilder(int collectionSize, PagingParams pagingParams, URI uri) {
        JsonArrayBuilder arrayBuilder = createArrayBuilder();
        if (collectionSize == pagingParams.size) {
            arrayBuilder.add(createNextLinkObject(pagingParams, uri));
        }
        if (pagingParams.start > 0) {
            arrayBuilder.add(createPreviousLinkObject(pagingParams, uri));
        }

        return arrayBuilder;
    }

    private JsonObject createNextLinkObject(PagingParams pagingParams, URI uri) {
        int next = pagingParams.start + pagingParams.size;
        URI nextUri = UriBuilder.fromUri(uri)
                .queryParam("start", next)
                .queryParam("size", pagingParams.size).build();

        return createLinkObject("next", nextUri);
    }

    private JsonObject createPreviousLinkObject(PagingParams pagingParams, URI uri) {
        int previous = pagingParams.start - pagingParams.size;
        if (previous < 0) {
            previous = 0;
        }
        URI previousUri = UriBuilder.fromUri(uri)
                .queryParam("start", previous)
                .queryParam("size", pagingParams.size).build();

        return createLinkObject("previous", previousUri);
    }

    private JsonObject createLinkObject(String relation, URI uri) {
        return createObjectBuilder().add("rel", createArrayBuilder().add(relation))
                .add("href", uri.toString()).build();
    }
}
