package net.f.store.business.customers.entity;

import net.f.store.business.AbstractValidatorIT;
import net.f.store.business.TestDataProducer;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class AddressIT extends AbstractValidatorIT {

    private Address aut;

    @Before
    public void initAddress() throws Exception {
        this.aut = TestDataProducer.getAddress();
    }

    @Test
    public void noConstraintViolation() throws Exception {
        Set<ConstraintViolation<Address>> result = this.vut.validate(this.aut);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void allPropertyValuesLessThanMinimumLimit() throws Exception {
        this.aut.setStreet("");
        this.aut.setCity("");
        this.aut.setState("");
        this.aut.setZipcode("");
        this.aut.setCountry("");

        Set<ConstraintViolation<Address>> result = this.vut.validate(this.aut);
//        displayConstraintViolations(result);

        int expected = 5;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void allPropertyValuesGreaterThanMaximumLimit() throws Exception {
        String string = String.valueOf(new char[101]);

        this.aut.setStreet(string);
        this.aut.setCity(string);
        this.aut.setState(string);
        this.aut.setZipcode(string);
        this.aut.setCountry(string);

        Set<ConstraintViolation<Address>> result = this.vut.validate(this.aut);
//        displayConstraintViolations(result);

        int expected = 5;
        assertThat(result.size(), is(expected));
    }

    @Test
    public void streetCityAreNull() throws Exception {
        this.aut = new Address();
        Set<ConstraintViolation<Address>> result = this.vut.validate(this.aut);
//        displayConstraintViolations(result);

        int expected = 4;
        assertThat(result.size(), is(expected));
    }
}