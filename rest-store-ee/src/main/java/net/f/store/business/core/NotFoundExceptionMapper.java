package net.f.store.business.core;

import net.f.store.business.core.entity.ErrorMessage;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Artyom Fyodorov
 */
@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {
    @Override
    public Response toResponse(NotFoundException ex) {
        int statusCode = ex.getResponse().getStatus();
        ErrorMessage errorMessage = ErrorMessage.of(ex.getLocalizedMessage(), statusCode);
        return Response.status(statusCode).entity(errorMessage).build();
    }
}
