package net.f.store.business.core.boundary;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

/**
 * The {@code PagingParams} it's a parameter aggregator that may be
 * used to inject into a resource method parameter.
 *
 * @author Artyom Fyodorov
 * @see javax.ws.rs.BeanParam
 */
public class PagingParams {
    @QueryParam("start")
    public int start;
    @QueryParam("size")
    @DefaultValue("2")
    public int size;

    public PagingParams() {
    }
}
