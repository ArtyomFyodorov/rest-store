package net.f.store.business.security.boundary;

import net.f.store.business.core.entity.ErrorMessage;

/**
 * @author Artyom Fyodorov
 */
class AuthenticationException extends RuntimeException {
    private static final int UNAUTHORIZED_STATUS_CODE = 401;
    private static final long serialVersionUID = -4660584556184556337L;
    private final ErrorMessage errorMessage;
    private final String realm;

    AuthenticationException(String message) {
        this(message, null);
    }

    AuthenticationException(String message, String realm) {
        super(message);
        this.errorMessage = ErrorMessage.of(getLocalizedMessage(), UNAUTHORIZED_STATUS_CODE);
        this.realm = realm;
    }

    String getRealm() {
        return realm;
    }

    ErrorMessage getErrorMessage() {
        return errorMessage;
    }
}
