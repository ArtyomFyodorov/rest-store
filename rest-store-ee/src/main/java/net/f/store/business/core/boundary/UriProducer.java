package net.f.store.business.core.boundary;

import net.f.store.business.cart.boundary.CartResource;
import net.f.store.business.cart.entity.LineItem;
import net.f.store.business.core.entity.AbstractEntity;
import net.f.store.business.customers.boundary.CustomersResource;
import net.f.store.business.customers.entity.Customer;
import net.f.store.business.orders.boundary.OrdersResource;
import net.f.store.business.orders.entity.Order;
import net.f.store.business.products.boundary.ProductsResource;
import net.f.store.business.products.entity.Product;

import javax.ws.rs.core.UriInfo;
import java.net.URI;

/**
 * @author Artyom Fyodorov
 */
public class UriProducer {

    public URI forCart(UriInfo uriInfo) {
        return createResourceUri(CartResource.class, uriInfo);
    }

    public URI forOrder(UriInfo uriInfo, Order order) {
        return createResourceUri(OrdersResource.class, order.getId(), "getOne", uriInfo);
    }

    public URI forOrders(UriInfo uriInfo) {
        return createResourceUri(OrdersResource.class, uriInfo);
    }

    public URI forCustomer(UriInfo uriInfo, Customer customer) {
        return createResourceUri(CustomersResource.class, customer.getId(), "getOne", uriInfo);
    }

    public URI forCustomers(UriInfo uriInfo) {
        return createResourceUri(CustomersResource.class, uriInfo);
    }

    public URI forProduct(UriInfo uriInfo, Product product) {
        return createResourceUri(ProductsResource.class, product.getId(), "getOne", uriInfo);
    }

    public URI forProducts(UriInfo uriInfo) {
        return createResourceUri(ProductsResource.class, uriInfo);
    }

    public URI forLineItem(UriInfo uriInfo, LineItem lineItem) {
        return createResourceUri(CartResource.class, lineItem.getId(), "updateLineItem", uriInfo);
    }

    //  ==============================================================================================================

    public URI createResourceUri(Class<?> resourceClass, Long id, String method, UriInfo uriInfo) {
        return uriInfo.getBaseUriBuilder().path(resourceClass).path(resourceClass, method).build(id);
    }

    public URI createResourceUri(Class<?> resourceClass, UriInfo uriInfo) {
        return uriInfo.getBaseUriBuilder().path(resourceClass).build();
    }

    public <T extends AbstractEntity> URI createResourceUri(UriInfo uriInfo, T resourceClass) {
        return uriInfo.getAbsolutePathBuilder().path(String.valueOf(resourceClass.getId())).build();
    }
}
