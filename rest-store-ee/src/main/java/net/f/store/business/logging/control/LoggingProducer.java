package net.f.store.business.logging.control;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.util.logging.Logger;

/**
 * @author Artyom Fyodorov
 */
public class LoggingProducer {

    /**
     * Creates injectable {@code Logger}s.
     * The log category of a {@code Logger} depends upon the class
     * of the object into which it's injected.
     *
     * @param ip an injection point what provides access to metadata.
     * @return a {@link java.util.logging.Logger} parameterized with
     * the injection point class name.
     */
    @Produces
    private Logger produce(InjectionPoint ip) {
        Class<?> clazz = ip.getMember().getDeclaringClass();
        return Logger.getLogger(clazz.getName());
    }
}
