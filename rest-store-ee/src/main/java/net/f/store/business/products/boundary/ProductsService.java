package net.f.store.business.products.boundary;

import net.f.store.business.core.boundary.Service;
import net.f.store.business.products.entity.Product;
import net.f.store.business.products.entity.Product_;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author Artyom Fyodorov
 */
@Stateless
public class ProductsService extends Service<Product, Long> {
    private @Inject EntityManager em;

    ProductsService() {
        super(Product.class);
    }

    @Override
    protected EntityManager getEM() {
        return this.em;
    }

    public List<Product> allByName(String name, int start, int size) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Product> cq = cb.createQuery(Product.class);
        Root<Product> p = cq.from(Product.class);
        cq.select(p);

        if (name != null) {
            Predicate predicate = cb.equal(p.get(Product_.name), name);
            cq.where(predicate);
        }

        TypedQuery<Product> tq = this.em.createQuery(cq);
        tq.setFirstResult(start);
        tq.setMaxResults(size);
        return tq.getResultList();
    }
}
