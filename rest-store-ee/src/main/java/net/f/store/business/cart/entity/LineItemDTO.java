package net.f.store.business.cart.entity;

import net.f.store.business.products.entity.Product;

import javax.validation.constraints.Min;
import javax.xml.bind.annotation.*;

/**
 * @author Artyom Fyodorov
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "line-item")
public class LineItemDTO {
    private int quantity;
    private Long productId;
    private Product product;

    @Min(value = 0, message = "{net.f.constraints.lineItem.quantity.Min.message}")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Min(value = 1, message = "{net.f.constraints.lineItemDTO.productId.Min.message}")
    @XmlElement(name = "product-code")
    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @XmlTransient
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
