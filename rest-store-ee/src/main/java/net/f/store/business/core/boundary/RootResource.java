package net.f.store.business.core.boundary;

import net.f.store.business.cart.boundary.CartResource;
import net.f.store.business.customers.boundary.CustomersResource;
import net.f.store.business.orders.boundary.OrdersResource;
import net.f.store.business.products.boundary.ProductsResource;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.net.URI;

/**
 * @author Artyom Fyodorov
 */
@Path("/")
@PermitAll
@Produces("application/json")
public class RootResource {

    @Context UriInfo info;
    @Inject JsonBuilder jsonBuilder;

    @HEAD
    public Response head() {
        UriBuilder baseUriBuilder = info.getBaseUriBuilder();
        URI customerUri = baseUriBuilder.clone().path(CustomersResource.class).build();
        URI productUri = baseUriBuilder.clone().path(ProductsResource.class).build();
        URI orderUri = baseUriBuilder.clone().path(OrdersResource.class).build();
        URI cartUri = baseUriBuilder.clone().path(CartResource.class).build();

        Link customersJson = Link.fromUri(customerUri).rel("customers").type("application/json").build();
        Link customersSirenJson = Link.fromUri(customerUri).rel("customers").type("application/vnd.siren+json").build();

        Link productsJson = Link.fromUri(productUri).rel("products").type("application/json").build();
        Link productsSirenJson = Link.fromUri(productUri).rel("products").type("application/vnd.siren+json").build();

        Link ordersJson = Link.fromUri(orderUri).rel("orders").type("application/json").build();
        Link ordersSirenJson = Link.fromUri(orderUri).rel("orders").type("application/vnd.siren+json").build();

        Link cartJson = Link.fromUri(cartUri).rel("cart").type("application/json").build();
        Link cartSirenJson = Link.fromUri(cartUri).rel("cart").type("application/vnd.siren+json").build();


        Response.ResponseBuilder builder = Response.ok()
                .links(
                        customersJson,
                        customersSirenJson,

                        productsJson,
                        productsSirenJson,

                        ordersJson,
                        ordersSirenJson,

                        cartJson,
                        cartSirenJson

                );

        return builder.build();
    }

    @GET
    public JsonObject store() {
        return jsonBuilder.buildStore(info);
    }
}
