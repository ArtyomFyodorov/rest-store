package net.f.store.business.orders;

import net.f.store.business.AbstractClient;
import net.f.store.business.BasicEncoder;
import net.f.store.business.PojoRepository;
import net.f.store.business.cart.LineItemDTO;
import net.f.store.business.customers.Customer;
import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonObject;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Response;
import java.net.URI;

import static javax.ws.rs.core.HttpHeaders.IF_NONE_MATCH;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * @author Artyom Fyodorov
 */
public class OrdersBoundaryIT extends AbstractClient {
    private String credentials;
    private URI ordersLocation;
    private URI customerLocation;
    private URI orderLocation;

    @Before
    public void init() throws Exception {
        this.customerLocation = createCustomer();
        this.orderLocation = createOrder();
    }

    private URI createCustomer() {
        URI customersLocation = getURI("customers");
        Customer dummyCustomer = PojoRepository.getCustomerWithRandomEmail();
        this.credentials = BasicEncoder.toAuthorizationHeaderValue(dummyCustomer.getEmail(), dummyCustomer.getPassword());
        return requestPOST(null, customersLocation, dummyCustomer).getLocation();
    }

    private URI createOrder() {
        this.ordersLocation = getURI("orders");
        URI cartLocation = getURI("cart");
        URI productLocation = getURI("products");
        String[] uriSegments = requestPOSTWithAdminRole(productLocation, PojoRepository.getProduct()).getLocation().toString().split("/");
        String productId = uriSegments[uriSegments.length - 1];  //  last path param
        LineItemDTO lineItem = LineItemDTO.newInstance(6, Long.parseLong(productId));
        requestPOST(credentials, cartLocation, lineItem);
        return getInvocationBuilderWithAuthHeader(credentials, ordersLocation).post(null).getLocation();
    }

    @After
    public void destroy() throws Exception {
        if (customerLocation != null) requestDELETEAdminRole(customerLocation);
    }

    @Test
    public void successfulCreate() throws Exception {
        //  GET
        Response response = requestGET(credentials, orderLocation, APPLICATION_JSON);
        JsonObject orderJson = response.readEntity(JsonObject.class);
        response.close();

        Order order = PojoRepository.getJsonAsOrder(orderJson);
        double actualTotalPrice = order.getGrandTotal();
        double expectedTotalPrice = 1499.94;  //  quantity = 6; cost = 249.99
        final double delta = 0.009;
        assertEquals(expectedTotalPrice, actualTotalPrice, delta);
    }

    @Test
    public void rejectsCreate_notAuthorized() throws Exception {
        Response response = getInvocationBuilderWithAuthHeader(null, ordersLocation).post(null);
        response.close();
        assertThat(response.getStatusInfo(), is(UNAUTHORIZED));
    }

    @Test
    public void rejectsCreate_authAsAdmin() throws Exception {
        //  try create / POST
        Response response = getInvocationBuilderWithAuthHeaderAdminRole(ordersLocation)
                .post(null);
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void rejectsGetAnOrder_notAuthorized() throws Exception {
        //  try / GET
        Response response = getInvocationBuilderWithAuthHeader(null, orderLocation).get();
        response.close();
        assertThat(response.getStatusInfo(), is(UNAUTHORIZED));
    }

    @Test
    public void rejectsGetAllOrders_notAuthorized() throws Exception {
        //  try get / GET
        Response response = getInvocationBuilderWithAuthHeader(null, ordersLocation).get();
        response.close();
        assertThat(response.getStatusInfo(), is(UNAUTHORIZED));
    }

    @Test
    public void rejectsOrderDoNotBelongToUser() throws Exception {
        //  prepare
        createCustomer();  //  create another dummy customer
        Response response = getInvocationBuilderWithAuthHeader(credentials, orderLocation).get();
        response.close();
        //  test
        assertThat(response.getStatusInfo(), Is.is(NOT_FOUND));
    }

    @Test
    public void successfulGetAnyOrder_authAsAdmin() throws Exception {
        Response response = requestGETWithAdminRole(orderLocation, APPLICATION_JSON);
        response.close();
        //  test
        assertThat(response.getStatusInfo(), Is.is(OK));
    }

    @Test
    public void rejectsCreate_cart_is_empty() throws Exception {
        //  prepare
        createCustomer();  //  create new dummy customer
        Response response = getInvocationBuilderWithAuthHeader(credentials, ordersLocation).post(null);
        response.close();
        //  test
        assertThat(response.getStatusInfo(), is(BAD_REQUEST));
    }

    @Test
    public void conditionalRequests() throws Exception {
        //  GET
        Response response = requestGET(credentials, orderLocation, APPLICATION_JSON);
        response.close();
        EntityTag eTag = response.getEntityTag();
        assertNotNull(eTag);
        //  send ETag with conditional GET
        response = getInvocationBuilderWithAuthHeader(credentials, orderLocation)
                .header(IF_NONE_MATCH, eTag).get();
        response.close();
        assertThat(response.getStatusInfo(), Is.is(NOT_MODIFIED));
    }
}