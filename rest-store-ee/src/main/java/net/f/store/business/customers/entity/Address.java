package net.f.store.business.customers.entity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Artyom Fyodorov
 */
@Embeddable
@Access(AccessType.PROPERTY)
public class Address {
    private String street;
    private String city;
    private String state;
    private String zipcode;
    private String country;

    @NotNull(message = "{net.f.constraints.address.street.NotNull.message}")
    @Size(min = 1, max = 100, message = "{net.f.constraints.address.street.Size.message}")
    @Column(length = 100, nullable = false)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @NotNull(message = "{net.f.constraints.address.city.NotNull.message}")
    @Size(min = 1, max = 100, message = "{net.f.constraints.address.city.Size.message}")
    @Column(length = 100, nullable = false)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Size(min = 1, max = 100, message = "{net.f.constraints.address.state.Size.message}")
    @Column(length = 100)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @NotNull(message = "{net.f.constraints.address.zipcode.NotNull.message}")
    @Size(min = 1, max = 30, message = "{net.f.constraints.address.zipcode.Size.message}")
    @Column(name = "zip_code", length = 30)
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @NotNull(message = "{net.f.constraints.address.country.NotNull.message}")
    @Size(min = 1, max = 100, message = "{net.f.constraints.address.country.Size.message}")
    @Column(length = 100)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
