package net.f.store.business.cart.boundary;

import net.f.store.business.cart.entity.Cart;
import net.f.store.business.cart.entity.LineItem;
import net.f.store.business.cart.entity.LineItemDTO;
import net.f.store.business.core.boundary.JsonBuilder;
import net.f.store.business.core.boundary.UriProducer;
import net.f.store.business.customers.boundary.CustomersService;
import net.f.store.business.customers.entity.Customer;
import net.f.store.business.products.boundary.ProductsService;
import net.f.store.business.products.entity.Product;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * @author Artyom Fyodorov
 */
public class CartResourceBean implements CartResource {
    private static final String NOT_FOUND_EXCEPTION_MESSAGE = "Could not get resource for path parameter: %d";
    private static final String BAD_REQUEST_EXCEPTION_MESSAGE = "Only %d available";

    @Context UriInfo info;
    @Context Request request;
    @Context SecurityContext securityContext;
    @Inject JsonBuilder jsonBuilder;
    @Inject UriProducer uriProducer;
    @Inject CartService cartService;
    @Inject ProductsService productsService;
    @Inject LineItemsService lineItemsService;
    @Inject CustomersService customersService;

    public Response get() {
        String userPrincipalName = securityContext.getUserPrincipal().getName();
        Cart cart = this.cartService.findOne(userPrincipalName).orElse(null);
        if (cart == null) {
            return null;  //  204 No Content
        }
        String cartVersion = String.valueOf(cart.getVersion());
        EntityTag eTag = new EntityTag(cartVersion);
        Response.ResponseBuilder rb = request.evaluatePreconditions(eTag);
        if (rb == null) {
            JsonObject cartJson = jsonBuilder.buildCart(cart, info);
            rb = Response.ok(cartJson).tag(eTag);
        }
        CacheControl cc = new CacheControl();
        cc.setMaxAge(86_400); // 24 h (hour)

        return rb.cacheControl(cc).build();
    }

    public Response addLineItem(LineItemDTO dto) {
        Long productId = dto.getProductId();
        Product product = getProduct(productId);

        int availableQuantity = product.getAvailableQuantity();
        int requiredQuantity = dto.getQuantity();
        if ((availableQuantity - requiredQuantity) < 0) {
            throw new BadRequestException(format(BAD_REQUEST_EXCEPTION_MESSAGE, availableQuantity));
        }
        dto.setProduct(product);
        Cart cart = this.cartService.saveToCart(getAuthCustomer(), dto);
        List<LineItem> lineItems = cart.getLineItems();
        LineItem lineItem = lineItems.get(lineItems.size() - 1); //  get last line item
        URI uri = uriProducer.createResourceUri(info, lineItem);

        return Response.created(uri).build();
    }

    public Response updateLineItem(Long lineItemId, LineItemDTO dto) {
        LineItem lineItem = getLineItem(lineItemId);

        String lineItemVersion = String.valueOf(lineItem.getVersion());
        EntityTag eTag = new EntityTag(lineItemVersion);
        Response.ResponseBuilder rb = request.evaluatePreconditions(eTag);
        if (rb == null) {
            cartService.updateLineItem(getAuthCustomer(), lineItem, dto);
            rb = Response.noContent();
        }

        return rb.build();
    }

    public void deleteLineItem(Long lineItemId) {
        LineItem lineItem = getLineItem(lineItemId);
        this.cartService.removeFromCart(getAuthCustomer(), lineItem);
    }

    private Customer getAuthCustomer() {
        return customersService.findByEmail(
                securityContext.getUserPrincipal().getName())
                .orElseThrow(() -> new ServerErrorException(INTERNAL_SERVER_ERROR));
    }

    private Product getProduct(Long productId) {
        return productsService.findOne(productId)
                .orElseThrow(() -> new NotFoundException(
                        format(NOT_FOUND_EXCEPTION_MESSAGE, productId)));
    }

    private LineItem getLineItem(Long lineItemId) {
        return lineItemsService.findOne(lineItemId)
                .orElseThrow(() -> new NotFoundException(
                        format(NOT_FOUND_EXCEPTION_MESSAGE, lineItemId)));
    }
}
