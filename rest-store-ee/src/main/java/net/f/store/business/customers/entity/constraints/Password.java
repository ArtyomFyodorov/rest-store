package net.f.store.business.customers.entity.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Artyom Fyodorov
 */
@NotNull(message = "{net.f.constraints.password.NotNull.message}")
@Size(min = 6, max = 128, message = "{net.f.constraints.password.Size.message}")
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {})
public @interface Password {

    String message() default "{net.f.constraints.password.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
