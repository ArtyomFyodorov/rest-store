package net.f.store.business.core.entity;

import net.f.store.business.TestDataProducer;
import net.f.store.business.cart.entity.LineItem;
import net.f.store.business.customers.entity.Customer;
import net.f.store.business.orders.entity.Order;
import net.f.store.business.products.entity.Product;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Artyom Fyodorov
 */
public class AbstractEntityIT {

    private static EntityManagerFactory emf;

    @BeforeClass
    public static void initEMF() {
        emf = Persistence.createEntityManagerFactory("it");
    }

    @Test
    public void productEqualityTest() {
        final Product shorts = TestDataProducer.getProduct(TestDataProducer.ProductType.CLOTHING_SHORTS);
        assertEqualityConsistency(Product.class, shorts);
    }

    @Test
    public void lineItemEqualityTest() {
        final LineItem lineItem = TestDataProducer.getLineItems().get(1);
        assertEqualityConsistency(LineItem.class, lineItem);
    }

    @Test
    public void customerEqualityTest() {
        final Customer customer = TestDataProducer.getCustomer();
        assertEqualityConsistency(Customer.class, customer);
    }

    @Test
    public void orderEqualityTest() {
        final Order order = TestDataProducer.getOrder();
        assertEqualityConsistency(Order.class, order);
    }

    private <T extends AbstractEntity> void assertEqualityConsistency(Class<T> clazz, T entity) {
        final Set<T> collection = new HashSet<>();
        // 1
        assertFalse(collection.contains(entity));
        // 2
        collection.add(entity);
        assertTrue(collection.contains(entity));
        // 3
        unitOfWork(entityManager -> {
            entityManager.persist(entity);
            entityManager.flush();
            assertTrue("The entity is not found in the Set after it's persisted.", collection.contains(entity));
        });
        // 4 The entity is found after the entity is detached
        assertTrue(collection.contains(entity));
        // 5
        unitOfWork(entityManager -> {
            T mergedEntity = entityManager.merge(entity);
            assertTrue("The entity is not found in the Set after it's merged.", collection.contains(mergedEntity));
        });
        // 6
        unitOfWork(entityManager -> {
            T wellKnownEntity = entityManager.find(clazz, entity.getId());
            assertTrue("The entity is not found in the Set after it's loaded in a Persistence Context.", collection.contains(wellKnownEntity));
        });
        // 7
        unitOfWork(entityManager -> {
            T foundEntity = entityManager.getReference(clazz, entity.getId());
            assertTrue("The entity is not found in the Set after it's loaded as a Proxy in the Persistence Context.", collection.contains(foundEntity));
        });
        // 8
        T deletedEntity = unitOfWork(entityManager -> {
            T deletableEntity = entityManager.getReference(clazz, entity.getId());
            entityManager.remove(deletableEntity);
            return deletableEntity;
        });
        assertTrue("The entity is not found in the Set after it's deleted.", collection.contains(deletedEntity));
    }

    private void unitOfWork(Consumer<EntityManager> consumer) {
        unitOfWork(entityManager -> {
            consumer.accept(entityManager);
            return null;
        });
    }

    private <T> T unitOfWork(Function<EntityManager, T> function) {
        T result;
        EntityManager em = null;

        try {
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            //
            tx.begin();
            result = function.apply(em);
            tx.commit();
        } finally {
            if (em != null) em.close();
        }

        return result;
    }
}
