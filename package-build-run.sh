#!/bin/bash
mvn clean install -Dmaven.test.skip=true && \
docker build -t poda/rest-store . && \
docker kill rest-store ; \
docker rm rest-store ; \
docker run -d -p 8080:8080 --name rest-store poda/rest-store
