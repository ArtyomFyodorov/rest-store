package net.f.store.business.products.entity;

import net.f.store.business.core.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Artyom Fyodorov
 */
@Entity
@Table(name = "products")
@Access(AccessType.PROPERTY)
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Product extends AbstractEntity {
    private String name;
    private double price;
    @XmlElement(name = "available")
    private int availableQuantity;

    @NotNull(message = "{net.f.constraints.product.name.NotNul.message}")
    @Size(min = 1, max = 255, message = "{net.f.constraints.product.name.Size.message}")
    @Column(nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @DecimalMin(value = "0.01", inclusive = true, message = "{net.f.constraints.product.cost.DecimalMin.message}")
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Min(value = 0, message = "{net.f.constraints.product.availableQuantity.Min.message}")
    @Column(name = "available")
    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    @Transient
    public boolean isAvailableForOrder() {
        return this.availableQuantity > 0;
    }

    @Transient
    public String availability() {
        return isAvailableForOrder() ? "In Stock" : "Out of Stock";
    }

    @Override
    public String toString() {
        return "Product{" +
                super.toString() +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", availableQuantity=" + availableQuantity +
                '}';
    }
}
