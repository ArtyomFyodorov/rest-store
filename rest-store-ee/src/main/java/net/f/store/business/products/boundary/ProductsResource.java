package net.f.store.business.products.boundary;

import net.f.store.business.core.boundary.PagingParams;
import net.f.store.business.products.entity.Product;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.json.JsonObject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

/**
 * @author Artyom Fyodorov
 */
@Path("/products")
@Produces({"application/json", "application/vnd.siren+json"})
@Consumes({"application/json", "application/vnd.siren+json"})
@PermitAll
public interface ProductsResource {

    @RolesAllowed("ADMIN")
    @POST
    Response create(@NotNull @Valid Product product);

    @GET
    @Path("/{id:[0-9][0-9]*}")
    Response getOne(@PathParam("id") Long productId, @Context Request request);

    @GET
    JsonObject getAll(@QueryParam("name") String name,
                      @BeanParam PagingParams pagingParams);
}
