package net.f.store.business.customers;

import net.f.store.business.AbstractClient;
import net.f.store.business.BasicEncoder;
import net.f.store.business.PojoRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonObject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static javax.ws.rs.core.HttpHeaders.IF_MATCH;
import static javax.ws.rs.core.HttpHeaders.IF_NONE_MATCH;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author Artyom Fyodorov
 */
public class CustomersBoundaryIT extends AbstractClient {
    private Customer dummyCustomer;
    private JsonObject dummyCustomerJson;
    private JsonObject customerWithRandomEmailJson;
    private String credentials;
    private URI customersLocation;
    private URI adminLocation;
    private URI customerLocation;
    private URI meLocation;

    @Before
    public void init() throws Exception {
        this.customerWithRandomEmailJson = PojoRepository.getCustomerAsJson(PojoRepository.getCustomerWithRandomEmail());
        this.customerLocation = createCustomer();
        this.adminLocation = new URI(customersLocation + "/2");
        this.meLocation = new URI(customersLocation + "/me");
    }

    @After
    public void cleanup() throws Exception {
        if (customerLocation != null) requestDELETEAdminRole(customerLocation);
    }

    private URI createCustomer() {
        this.customersLocation = getURI("customers");
        this.dummyCustomer = PojoRepository.getCustomerWithRandomEmail();
        this.dummyCustomerJson = PojoRepository.getCustomerAsJson(dummyCustomer);
        this.credentials = BasicEncoder.toAuthorizationHeaderValue(dummyCustomer.getEmail(), dummyCustomer.getPassword());
        return requestPOST(null, customersLocation, dummyCustomer).getLocation();
    }

    private Set<URI> createCustomers(int numberOfCustomers) {
        Set<URI> locations = new HashSet<>();
        //  create / POST
        IntStream.range(0, numberOfCustomers)
                .forEach(value -> {
                    URI location = createCustomer();
                    assertNotNull(location);
                    locations.add(location);
                });

        return locations;
    }

    @Test
    public void successfulCreate() throws Exception {
        //  get after creation / GET
        Response response = requestGET(credentials, meLocation, APPLICATION_JSON);
        JsonObject customerJson = response.readEntity(JsonObject.class);
        response.close();
        //  test
        Customer customer = PojoRepository.getJsonAsCustomer(customerJson);
        String actualEmailAddress = customer.getEmail();
        String expectedEmailAddress = dummyCustomer.getEmail();
        assertThat(actualEmailAddress, is(expectedEmailAddress));
    }

    @Test
    public void rejectsEmailAlreadyExists() throws Exception {
        //  try create / POST
        Response response = getInvocationBuilderWithAuthHeader(null, customersLocation)
                .post(Entity.json(dummyCustomerJson));
        response.close();
        //  test
        assertThat(response.getStatusInfo(), is(CONFLICT));
    }

    @Test
    public void rejectsGetAnyCustomer_authAsUser() throws Exception {
        Response response = getInvocationBuilderWithAuthHeader(credentials, customerLocation).get();
        //  test
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void successfulGetMyself_authAsUser() throws Exception {
        Response response = requestGET(credentials, meLocation, APPLICATION_JSON);
        //  test
        assertThat(response.getStatusInfo(), is(OK));
    }

    @Test
    public void successfulGetAnyCustomer_authAsAdmin() throws Exception {
        Response response = requestGETWithAdminRole(customerLocation, APPLICATION_JSON);
        //  test
        assertThat(response.getStatusInfo(), is(OK));
    }

    @Test
    public void rejectsGetAllCustomers_authAsUser() throws Exception {
        Response response = getInvocationBuilderWithAuthHeader(credentials, customersLocation).get();
        //  test
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void successfulGetAllCustomers_autAsAdmin() throws Exception {
        Response response = requestGETWithAdminRole(customersLocation, APPLICATION_JSON);
        //  test
        assertThat(response.getStatusInfo(), is(OK));
    }

    @Test
    public void rejectsAdminDeletion_authAsUser() throws Exception {
        //  try delete / DELETE
        Response response = getInvocationBuilderWithAuthHeader(credentials, adminLocation).delete();
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void rejectsAdminDeletion_authAsAdmin() throws Exception {
        //  try delete / DELETE
        Response response = getInvocationBuilderWithAuthHeaderAdminRole(adminLocation).delete();
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void rejectsUserDeletion_authAsUser() throws Exception {
        //  prepare
        createCustomer();  //  create another dummy customer
        //  try delete / DELETE
        Response response = getInvocationBuilderWithAuthHeader(credentials, customerLocation).delete();
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void rejectsDeletion_user_not_found() throws Exception {
        //  prepare
        URI notExistingCustomerLocation = URI.create(customersLocation + "/100500");
        //  try delete / DELETE
        Response response = getInvocationBuilderWithAuthHeader(credentials, notExistingCustomerLocation).delete();
        response.close();
        assertThat(response.getStatusInfo(), is(NOT_FOUND));
    }

    @Test
    public void userSuccessfullyDeleted_AuthAsSameUser() throws Exception {
        //  DELETE
        Response response = getInvocationBuilderWithAuthHeader(credentials, customerLocation).delete();
        response.close();
        customerLocation = null;  // see if statement in the cleanup method.
        assertThat(response.getStatusInfo(), is(NO_CONTENT));
    }

    @Test
    public void userSuccessfullyDeleted_AuthAsAdmin() throws Exception {
        //  DELETE
        Response response = getInvocationBuilderWithAuthHeaderAdminRole(customerLocation).delete();
        response.close();
        customerLocation = null;  // see if statement in the cleanup method.
        assertThat(response.getStatusInfo(), is(NO_CONTENT));
    }

    @Test
    public void userSuccessfullyUpdated_authAsAdmin() throws Exception {
        //  PUT
        Response response = getInvocationBuilderWithAuthHeaderAdminRole(adminLocation)
                .put(Entity.json(customerWithRandomEmailJson));
        response.close();
        assertThat(response.getStatusInfo(), is(NO_CONTENT));
    }

    @Test
    public void userSuccessfullyUpdated_authAsSameUser() throws Exception {
        //  PUT
        Response response = getInvocationBuilderWithAuthHeader(credentials, customerLocation)
                .put(Entity.json(customerWithRandomEmailJson));
        response.close();
        assertThat(response.getStatusInfo(), is(NO_CONTENT));
    }

    @Test
    public void rejectsUserUpdate_authAsUser() throws Exception {
        //  prepare
        createCustomer();  //  credentials has been updated
        //  try update / PUT
        Response response = getInvocationBuilderWithAuthHeader(credentials, customerLocation)
                .put(Entity.json(customerWithRandomEmailJson));
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void adminSuccessfullyUpdated_authAsAdmin() throws Exception {
        //  PUT
        Response response = getInvocationBuilderWithAuthHeaderAdminRole(adminLocation)
                .put(Entity.json(customerWithRandomEmailJson));
        response.close();

        assertThat(response.getStatusInfo(), is(NO_CONTENT));
    }

    @Test
    public void rejectsAdminUpdate_authAsUser() throws Exception {
        //  try update / PUT
        Response response = getInvocationBuilderWithAuthHeader(credentials, adminLocation)
                .put(Entity.json(customerWithRandomEmailJson));
        response.close();
        assertThat(response.getStatusInfo(), is(FORBIDDEN));
    }

    @Test
    public void testCustomersRequestParams() throws Exception {
        //  preparation / request
        final URI requestURI = client.target(customersLocation)
                .queryParam("start", "0")
                .queryParam("size", "100500")   //  By default, the size of the requested page is 2.
                .getUri();
        //  get before creation / GET
        Response response = requestGETWithAdminRole(requestURI, APPLICATION_JSON);  //  Only admins are allowed to get all customers.
        JsonObject customersJson = response.readEntity(JsonObject.class);
        List<Customer> customers = PojoRepository.getJsonAsCustomerList(customersJson);
        final int beforeTotalCustomers = customers.size();
        response.close();

        Set<URI> locations = new HashSet<>();
        try {
            final int numberOfNewCustomers = 18;
            locations = createCustomers(numberOfNewCustomers);
            //  get after creation / GET
            response = requestGETWithAdminRole(requestURI, APPLICATION_JSON);  //  Only admins are allowed to get all customers.
            customersJson = response.readEntity(JsonObject.class);
            customers = PojoRepository.getJsonAsCustomerList(customersJson);
            response.close();
            //  test
            final int afterTotalCustomers = customers.size();
            final int expectedTotalCustomers = beforeTotalCustomers + numberOfNewCustomers;
            assertThat(afterTotalCustomers, is(expectedTotalCustomers));
        } finally {
            requestDELETEAdminRole(locations.toArray(new URI[0]));
        }
    }

    @Test
    public void crud() throws Exception {
        //  GET
        Response response = requestGET(credentials, meLocation, APPLICATION_JSON);
        JsonObject customerJson = response.readEntity(JsonObject.class);
        Customer updateableCustomer = PojoRepository.getJsonAsCustomer(customerJson);
        System.out.println(updateableCustomer);
        response.close();
        //  update / PUT
        updateableCustomer.setEmail("updated@email.com");
        updateableCustomer.setPassword(dummyCustomer.getPassword());
        requestPUT(credentials, customerLocation, updateableCustomer);
        //  get after updating / GET
        String newCredentials = BasicEncoder.toAuthorizationHeaderValue(
                //  Encoding new value for 'Authorization' header, because username has been updated.
                updateableCustomer.getEmail(), updateableCustomer.getPassword());
        response = requestGET(newCredentials, meLocation, APPLICATION_JSON);
        customerJson = response.readEntity(JsonObject.class);
        response.close();
        Customer updatedCustomer = PojoRepository.getJsonAsCustomer(customerJson);
        //  test
        String expectedEmail = updateableCustomer.getEmail();
        String actualEmail = updatedCustomer.getEmail();
        assertThat(actualEmail, is(expectedEmail));
    }

    @Test
    public void conditionalRequests() throws Exception {
        //  get after creation  / GET
        Response response = requestGET(credentials, meLocation, APPLICATION_JSON);
        JsonObject customerJson = response.readEntity(JsonObject.class);
        Customer customer = PojoRepository.getJsonAsCustomer(customerJson);
        response.close();
        EntityTag eTag = response.getEntityTag();
        assertNotNull(eTag);
        //  send ETag with Conditional GET
        response = getInvocationBuilderWithAuthHeader(credentials, meLocation)
                .header(IF_NONE_MATCH, eTag).get();
        response.close();
        assertThat(response.getStatusInfo(), is(NOT_MODIFIED));
        //  update and send a bad ETag with Conditional PUT
        customer.setEmail("tryupdate@email.com");
        customer.setPassword(dummyCustomer.getPassword());
        customerJson = PojoRepository.getCustomerAsJson(customer);
        response = getInvocationBuilderWithAuthHeader(credentials, customerLocation)
                .header(IF_MATCH, "DUKE")
                .put(Entity.json(customerJson));
        response.close();
        //  test
        assertThat(response.getStatusInfo(), is(PRECONDITION_FAILED));
    }
}