package net.f.store.business.core;

import javax.ejb.EJBException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;

/**
 * @author Artyom Fyodorov
 */
@Provider
public class EJBExceptionMapper implements ExceptionMapper<EJBException> {
    @Context Providers providers;

    @SuppressWarnings("unchecked")
    @Override
    public Response toResponse(EJBException ex) {
        if (ex.getCausedByException() == null) {
            return Response.serverError().build();
        }
        Class cause = ex.getCausedByException().getClass();
        ExceptionMapper exMapper = providers.getExceptionMapper(cause);
        if (exMapper == null) {
            return Response.serverError().build();
        } else {
            return exMapper.toResponse(ex.getCausedByException());
        }

    }
}
