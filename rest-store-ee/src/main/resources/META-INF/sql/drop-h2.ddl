drop table customers if exists
drop table jng_cart_line_item if exists
drop table jng_order_line_item if exists
drop table line_items if exists
drop table orders if exists
drop table products if exists
drop table shopping_cart if exists
drop sequence if exists hibernate_sequence
