package net.f.store.business.security.control;

import org.junit.Test;

import java.util.stream.IntStream;

import static net.f.store.business.security.control.Digester.digest;
import static org.junit.Assert.assertEquals;

/**
 * @author Artyom Fyodorov
 */
public class DigesterTest {
    private static final String PASSWORD = "Java20";

    @Test
    public void successfulHashComputation() throws Exception {
        IntStream.range(0, 10)
                .forEach(value -> assertEquals(digest(PASSWORD), digest(PASSWORD)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void rejectsCharsetIsNull() throws Exception {
        digest("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void rejectsPasswordIsInvalid() throws Exception {
        digest(PASSWORD, null);
    }

}